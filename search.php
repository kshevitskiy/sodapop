<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package sodapop
 */

get_header(); ?>

		<main id="main" class="site-main">

		<?php
		if ( have_posts() ) : ?>

			<header class="page-header">
				<div class="container">
					<div class="text-center">						
						<h1 class="page-title"><?php
							/* translators: %s: search query. */
							printf( esc_html__( 'Search Results for: %s', 'sodapop' ), '<span>' . get_search_query() . '</span>' );
						?></h1>
					</div>
				</div>
			</header><!-- .page-header -->

			<div class="page-section">
				<div class="container">					
					<div class="row">
						<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
							<?php
								get_search_form();
							?>
						</div>
					</div>
				</div>
			</div>			

			<div class="page-section">
				<div class="container">
					
					<?php
					/* Start the Loop */
					while ( have_posts() ) : the_post();

						/**
						 * Run the loop for the search to output the results.
						 * If you want to overload this in a child theme then include a file
						 * called content-search.php and that will be used instead.
						 */
						get_template_part( 'template-parts/content', 'search' );

					endwhile;
					?>

				</div>			
			</div>

			<?php
			the_posts_navigation();

		else :

			get_template_part( 'template-parts/content', 'none' );

		endif; ?>

		</main><!-- #main -->

<?php
get_sidebar();
get_footer();
