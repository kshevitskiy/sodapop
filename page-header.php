<?php
/**
 * The template for displaying page header
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sodapop
 */
?>

<?php
    $color = get_field('header_headline_color');
    $bg_color = get_field('header_background_color');
    $bg_image = get_field('header_background_image');
?>

<header class="page-header" 
    <?php
    if( !empty( $bg_color ) || !empty( $bg_image ) ) :
        echo 'style="background-color: '. $bg_color .'; background-image: url('. $bg_image .');"';                                
    endif;
    ?>     
>
    <div class="text-center">
        <?php
        if( !empty( $color ) ) { ?>
            <?php the_title( '<h1 class="page-header__title text-uppercase" style="color: '. $color .'">', '</h1>' ); ?>
        <?php 
        } else { 
        ?>
            <?php the_title( '<h1 class="page-header__title text-uppercase">', '</h1>' ); ?>
        <?php
        } 
        ?>
    </div>
</header>