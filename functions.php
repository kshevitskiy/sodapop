<?php
/**
 * sodapop functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package sodapop
 */

if ( ! function_exists( 'sodapop_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function sodapop_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on sodapop, use a find and replace
		 * to change 'sodapop' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'sodapop', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
		/*
		 * Unit
		 */
		add_image_size( 'unit-thumb', 600, 450 );		
		add_image_size( 'unit-image', 700, 800 );		
		/*
		 * Essence
		 */
		add_image_size( 'essence-thumb-mobile', 140, 180 );
		add_image_size( 'essence-thumb', 150, 200 );
		add_image_size( 'essence-image', 220, 480 );
		/*
		 * Product grid		 
		 */
		add_image_size( 'product-grid', 150, 200 );
		/*
		 * Press
		 */
		add_image_size( 'press-thumb', 280, 200 );		

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'sodapop' ),
			'menu-2' => esc_html__( 'Footer widget', 'sodapop' ),
			'menu-3' => esc_html__( 'Footer widget', 'sodapop' ),
			'menu-4' => esc_html__( 'Social', 'sodapop' ),
			'menu-5' => esc_html__( 'Change language', 'sodapop' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'sodapop_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'sodapop_setup' );

/**
 * Change custom logo class
 */

add_filter( 'get_custom_logo', 'logo_class_name' );

function logo_class_name( $html ) {
    $html = str_replace( 'custom-logo', 'logo', $html );
    return $html;
}

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function sodapop_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'sodapop_content_width', 640 );
}
add_action( 'after_setup_theme', 'sodapop_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */

function sodapop_widgets_init() {	

	$sidebars = array('Sidebar', 'Footer widgets');
	$i = 0;
	foreach($sidebars as $sb) {
		$i++;
		register_sidebar( array(
			'name'          => esc_html__( " $sb ", 'sodapop' ),
			'id'            => 'sidebar-'. $i .'',
			'description'   => esc_html__( 'Add widgets here.', 'sodapop' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s col-xs-12 col-sm-6 col-md-4">',
			'after_widget'  => '</section>',
			'before_title'  => '<h4 class="widget__title text-uppercase">',
			'after_title'   => '</h4>',
		) );
	}

	register_sidebar( array(
		'name'          => esc_html__( 'Navigation widget', 'sodapop' ),
		'id'            => 'sidebar-3',
		'description'   => esc_html__( 'Add widgets here.', 'sodapop' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4 class="site-menu-contacts__title u-text-xs text-uppercase">',
		'after_title'   => '</h4>',
	) );
}

add_action( 'widgets_init', 'sodapop_widgets_init' );

/** 
 * jQuery init
 */
function jquery_init()   
{  
    if (!is_admin())   
    {  
        wp_deregister_script('jquery');  
        wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js', FALSE, '1.11.0', TRUE);  
        wp_enqueue_script('jquery');  
    }  
}
add_action('init', 'jquery_init'); 

/**
 * Enqueue scripts and styles.
 */
function sodapop_scripts() {
	wp_enqueue_style( 'sodapop-style', get_stylesheet_uri() );
	wp_enqueue_style( 'sodapop-bootstrap-style', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css', array(), '3.3.7', 'all' );
	// wp_enqueue_style( 'sodapop-swiper-style', '//cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.6/css/swiper.min.css', array(), '4.0.6', 'all' );
	wp_enqueue_style( 'sodapop-main-style', get_template_directory_uri() . '/css/style.css', array(), '1.0', 'all' );


	wp_enqueue_script('jquery');
	wp_enqueue_script( 'sodapop-bootstrap-js', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array(), '3.3.7', true );
	// wp_enqueue_script( 'sodapop-modernizr-js', '//cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', array(), '2.8.3', true );
	wp_enqueue_script( 'sodapop-swiper-js', '//cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.6/js/swiper.min.js', array(), '4.0.6', true );	
	// wp_enqueue_script( 'sodapop-isotope-js', '//unpkg.com/isotope-layout@3/dist/isotope.pkgd.min.js', array(), '3.0.0', true );	
	wp_enqueue_script( 'sodapop-main-js', get_template_directory_uri() . '/js/main.js', array(), '1.0', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'sodapop_scripts' );


/**
 * Enqueue Google Fonts
 */
// function sodapop_fonts() {
// 	wp_enqueue_style( 'sodapop-fonts', 'https://fonts.googleapis.com/css?family=Poppins:400,600,700,900', true ); 
// }
// add_action( 'wp_enqueue_scripts', 'sodapop_fonts' );


function sodapop_fonts() {
    if( !wp_script_is( 'sodapop-fonts', 'done' ) ) {
        echo '<script>
			WebFontConfig = { google: { families: [ "Poppins:400,600,700,900" ] } };
			(function() {
				var wf = document.createElement("script");
				wf.src = ("https:" == document.location.protocol ? "https" : "http") + "://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js";
				wf.type = "text/javascript";
				wf.async = "true";
				var s = document.getElementsByTagName("script")[0];
				s.parentNode.insertBefore(wf, s);
			})();
        </script>';
        global $g_fonts;
        $g_fonts->done[] = 'sodapop-fonts';
    }
}
add_action( 'wp_footer', sodapop_fonts );


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}


/*
 * Limit excerpt and content
 */
function excerpt($limit) {
      $excerpt = explode(' ', get_the_excerpt(), $limit);

      if (count($excerpt) >= $limit) {
          array_pop($excerpt);
          $excerpt = implode(" ", $excerpt) . '...';
      } else {
          $excerpt = implode(" ", $excerpt);
      }

      $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);

      return $excerpt;
}

function content($limit) {
    $content = explode(' ', get_the_content(), $limit);

    if (count($content) >= $limit) {
        array_pop($content);
        $content = implode(" ", $content) . '...';
    } else {
        $content = implode(" ", $content);
    }

    $content = preg_replace('/\[.+\]/','', $content);
    $content = apply_filters('the_content', $content); 
    $content = str_replace(']]>', ']]&gt;', $content);

    return $content;
}


/*
 * Hide ACF
 */
function remove_acf_menu() {
	remove_menu_page('edit.php?post_type=acf');
}
add_action( 'admin_menu', 'remove_acf_menu', 999);


/*
 * Disable wp-embed js
 */
function my_deregister_scripts(){
  wp_deregister_script( 'wp-embed' );
}
add_action( 'wp_footer', 'my_deregister_scripts' );


/**
 * Disable the emoji's
 */
function disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
	add_filter( 'wp_resource_hints', 'disable_emojis_remove_dns_prefetch', 10, 2 );
}
add_action( 'init', 'disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 * 
 * @param array $plugins 
 * @return array Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}

/**
 * Remove emoji CDN hostname from DNS prefetching hints.
 *
 * @param array $urls URLs to print for resource hints.
 * @param string $relation_type The relation type the URLs are printed for.
 * @return array Difference betwen the two arrays.
 */
function disable_emojis_remove_dns_prefetch( $urls, $relation_type ) {
	if ( 'dns-prefetch' == $relation_type ) {
		/** This filter is documented in wp-includes/formatting.php */
		$emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );	
		$urls = array_diff( $urls, array( $emoji_svg_url ) );
 	}
	return $urls;
}


/*
 * Return an alternate title, without prefix, for every type used in the get_the_archive_title()
 */
add_filter('get_the_archive_title', function ($title) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    } elseif ( is_tag() ) {
        $title = single_tag_title( '', false );
    } elseif ( is_author() ) {
        $title = '<span class="vcard">' . get_the_author() . '</span>';
    } elseif ( is_year() ) {
        $title = get_the_date( _x( 'Y', 'yearly archives date format' ) );
    } elseif ( is_month() ) {
        $title = get_the_date( _x( 'F Y', 'monthly archives date format' ) );
    } elseif ( is_day() ) {
        $title = get_the_date( _x( 'F j, Y', 'daily archives date format' ) );
    } elseif ( is_tax( 'post_format' ) ) {
        if ( is_tax( 'post_format', 'post-format-aside' ) ) {
            $title = _x( 'Asides', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) {
            $title = _x( 'Galleries', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-image' ) ) {
            $title = _x( 'Images', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-video' ) ) {
            $title = _x( 'Videos', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-quote' ) ) {
            $title = _x( 'Quotes', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-link' ) ) {
            $title = _x( 'Links', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-status' ) ) {
            $title = _x( 'Statuses', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-audio' ) ) {
            $title = _x( 'Audio', 'post format archive title' );
        } elseif ( is_tax( 'post_format', 'post-format-chat' ) ) {
            $title = _x( 'Chats', 'post format archive title' );
        }
    } elseif ( is_post_type_archive() ) {
        $title = post_type_archive_title( '', false );
    } elseif ( is_tax() ) {
        $title = single_term_title( '', false );
    } else {
        $title = __( 'Archives' );
    }
    return $title;
});


/*
 * CF7 fixes
 */
function dvk_dequeue_scripts() {

    $load_scripts = false;

    if( is_singular() ) {
    	$post = get_post();

    	if( has_shortcode($post->post_content, 'contact-form-7') ) {
        	$load_scripts = true;
    	}

    }

    if( ! $load_scripts ) {
        wp_dequeue_script( 'contact-form-7' );
    }

}
add_action( 'wp_enqueue_scripts', 'dvk_dequeue_scripts', 99 );


/*
 * Pagination
 */
function pagination() {
 
    if( is_singular() )
        return;
 
    global $wp_query;
 
    /** Stop execution if there's only 1 page */
    if( $wp_query->max_num_pages <= 1 )
        return;
 
    $paged = get_query_var( 'paged' ) ? absint( get_query_var( 'paged' ) ) : 1;
    $max   = intval( $wp_query->max_num_pages );
 
    /** Add current page to the array */
    if ( $paged >= 1 )
        $links[] = $paged;
 
    /** Add the pages around the current page to the array */
    if ( $paged >= 3 ) {
        $links[] = $paged - 1;
        $links[] = $paged - 2;
    }
 
    if ( ( $paged + 2 ) <= $max ) {
        $links[] = $paged + 2;
        $links[] = $paged + 1;
    }
 
    echo '<div class="page-section"><div class="container text-center"><ul class="pagination">' . "\n";
 
    /** Previous Post Link */
    if ( get_previous_posts_link() )
        printf( '<li class="hidden-xs">%s</li>' . "\n", get_previous_posts_link('&larr;') );
 
    /** Link to first page, plus ellipses if necessary */
    if ( ! in_array( 1, $links ) ) {
        $class = 1 == $paged ? ' class="active"' : '';
 
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( 1 ) ), '1' );
 
        if ( ! in_array( 2, $links ) )
            echo '<li><a href="javascript:void(0)">…</a></li>';
    }
 
    /** Link to current page, plus 2 pages in either direction if necessary */
    sort( $links );
    foreach ( (array) $links as $link ) {
        $class = $paged == $link ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $link ) ), $link );
    }
 
    /** Link to last page, plus ellipses if necessary */
    if ( ! in_array( $max, $links ) ) {
        if ( ! in_array( $max - 1, $links ) )
            echo '<li><a href="javascript:void(0)">…</a></li>' . "\n";
 
        $class = $paged == $max ? ' class="active"' : '';
        printf( '<li%s><a href="%s">%s</a></li>' . "\n", $class, esc_url( get_pagenum_link( $max ) ), $max );
    }
 
    /** Next Post Link */
    if ( get_next_posts_link() )
        printf( '<li class="hidden-xs">%s</li>' . "\n", get_next_posts_link('&rarr;') );
 
    echo '</ul></div></div>' . "\n";
 
}


/*
 * Google maps
 */
function google_map_api( $api ){
	$api['key'] = 'AIzaSyCWAK71Qa7T-e8lkDBpReByGPyfJuclg8k';
	return $api;	
}
add_filter('acf/fields/google_map/api', 'google_map_api');


/**
 * Custom post types
 */

/* 
 * Slider 
 */
function slider() {

	$labels = array(
		'name'                  => _x( 'Slides', 'Post Type General Name', 'sodapop' ),
		'singular_name'         => _x( 'Slide', 'Post Type Singular Name', 'sodapop' ),
		'menu_name'             => __( 'Slider', 'sodapop' ),
		'name_admin_bar'        => __( 'Slide', 'sodapop' ),
		'archives'              => __( 'Slides Archives', 'sodapop' ),
		'attributes'            => __( 'Slide Attributes', 'sodapop' ),
		'parent_item_colon'     => __( 'Parent Slide:', 'sodapop' ),
		'all_items'             => __( 'All Slides', 'sodapop' ),
		'add_new_item'          => __( 'Add New Slide', 'sodapop' ),
		'add_new'               => __( 'Add New', 'sodapop' ),
		'new_item'              => __( 'New Slide', 'sodapop' ),
		'edit_item'             => __( 'Edit Slide', 'sodapop' ),
		'update_item'           => __( 'Update Slide', 'sodapop' ),
		'view_item'             => __( 'View Slide', 'sodapop' ),
		'view_items'            => __( 'View Slides', 'sodapop' ),
		'search_items'          => __( 'Search Slide', 'sodapop' ),
		'not_found'             => __( 'Not found', 'sodapop' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'sodapop' ),
		'featured_image'        => __( 'Slide image', 'sodapop' ),
		'set_featured_image'    => __( 'Set slide image', 'sodapop' ),
		'remove_featured_image' => __( 'Remove slide image', 'sodapop' ),
		'use_featured_image'    => __( 'Use as slide image', 'sodapop' ),
		'insert_into_item'      => __( 'Insert into slide', 'sodapop' ),
		'uploaded_to_this_item' => __( 'Uploaded to this slide', 'sodapop' ),
		'items_list'            => __( 'Slides list', 'sodapop' ),
		'items_list_navigation' => __( 'Slides list navigation', 'sodapop' ),
		'filter_items_list'     => __( 'Filter slides list', 'sodapop' ),
	);
	$args = array(
		'label'                 => __( 'Slide', 'sodapop' ),
		'description'           => __( 'Post Type Description', 'sodapop' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail', ),
		'taxonomies'            => array(),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'slide', $args );

}
add_action( 'init', 'slider', 0 );


/* 
 * Slider about
 */
function slider_about() {

	$labels = array(
		'name'                  => _x( 'Slides', 'Post Type General Name', 'sodapop' ),
		'singular_name'         => _x( 'Slide', 'Post Type Singular Name', 'sodapop' ),
		'menu_name'             => __( 'About us slider', 'sodapop' ),
		'name_admin_bar'        => __( 'About us slide', 'sodapop' ),
		'archives'              => __( 'Slides Archives', 'sodapop' ),
		'attributes'            => __( 'Slide Attributes', 'sodapop' ),
		'parent_item_colon'     => __( 'Parent Slide:', 'sodapop' ),
		'all_items'             => __( 'All Slides', 'sodapop' ),
		'add_new_item'          => __( 'Add New Slide', 'sodapop' ),
		'add_new'               => __( 'Add New', 'sodapop' ),
		'new_item'              => __( 'New Slide', 'sodapop' ),
		'edit_item'             => __( 'Edit Slide', 'sodapop' ),
		'update_item'           => __( 'Update Slide', 'sodapop' ),
		'view_item'             => __( 'View Slide', 'sodapop' ),
		'view_items'            => __( 'View Slides', 'sodapop' ),
		'search_items'          => __( 'Search Slide', 'sodapop' ),
		'not_found'             => __( 'Not found', 'sodapop' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'sodapop' ),
		'featured_image'        => __( 'Slide image', 'sodapop' ),
		'set_featured_image'    => __( 'Set slide image', 'sodapop' ),
		'remove_featured_image' => __( 'Remove slide image', 'sodapop' ),
		'use_featured_image'    => __( 'Use as slide image', 'sodapop' ),
		'insert_into_item'      => __( 'Insert into slide', 'sodapop' ),
		'uploaded_to_this_item' => __( 'Uploaded to this slide', 'sodapop' ),
		'items_list'            => __( 'Slides list', 'sodapop' ),
		'items_list_navigation' => __( 'Slides list navigation', 'sodapop' ),
		'filter_items_list'     => __( 'Filter slides list', 'sodapop' ),
	);
	$args = array(
		'label'                 => __( 'Slide', 'sodapop' ),
		'description'           => __( 'Post Type Description', 'sodapop' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail', ),
		'taxonomies'            => array(),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => true,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'slide-about', $args );

}
add_action( 'init', 'slider_about', 0 );

/**
 * Units
 */
function units() {

	$labels = array(
		'name'                  => _x( 'Units', 'Post Type General Name', 'sodapop' ),
		'singular_name'         => _x( 'Unit', 'Post Type Singular Name', 'sodapop' ),
		'menu_name'             => __( 'Units', 'sodapop' ),
		'name_admin_bar'        => __( 'Unit', 'sodapop' ),
		'archives'              => __( 'Units Archives', 'sodapop' ),
		'attributes'            => __( 'Unit Attributes', 'sodapop' ),
		'parent_item_colon'     => __( 'Parent Unit:', 'sodapop' ),
		'all_items'             => __( 'All Units', 'sodapop' ),
		'add_new_item'          => __( 'Add New Unit', 'sodapop' ),
		'add_new'               => __( 'Add New', 'sodapop' ),
		'new_item'              => __( 'New Unit', 'sodapop' ),
		'edit_item'             => __( 'Edit Unit', 'sodapop' ),
		'update_item'           => __( 'Update Unit', 'sodapop' ),
		'view_item'             => __( 'View Unit', 'sodapop' ),
		'view_items'            => __( 'View Units', 'sodapop' ),
		'search_items'          => __( 'Search Unit', 'sodapop' ),
		'not_found'             => __( 'Not found', 'sodapop' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'sodapop' ),
		'featured_image'        => __( 'Unit image', 'sodapop' ),
		'set_featured_image'    => __( 'Set unit image', 'sodapop' ),
		'remove_featured_image' => __( 'Remove unit image', 'sodapop' ),
		'use_featured_image'    => __( 'Use as unit image', 'sodapop' ),
		'insert_into_item'      => __( 'Insert into unit', 'sodapop' ),
		'uploaded_to_this_item' => __( 'Uploaded to this unit', 'sodapop' ),
		'items_list'            => __( 'Units list', 'sodapop' ),
		'items_list_navigation' => __( 'Units list navigation', 'sodapop' ),
		'filter_items_list'     => __( 'Filter units list', 'sodapop' ),
	);
	$args = array(
		'label'                 => __( 'Unit', 'sodapop' ),
		'description'           => __( 'Post Type Description', 'sodapop' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', ),
		'taxonomies'            => array( 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'units',		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'unit', $args );

}
add_action( 'init', 'units', 0 );

/**
 * Essences
 */
function essences() {

	$labels = array(
		'name'                  => _x( 'Essences', 'Post Type General Name', 'sodapop' ),
		'singular_name'         => _x( 'Essence', 'Post Type Singular Name', 'sodapop' ),
		'menu_name'             => __( 'Essences', 'sodapop' ),
		'name_admin_bar'        => __( 'Essence', 'sodapop' ),
		'archives'              => __( 'Essences Archives', 'sodapop' ),
		'attributes'            => __( 'Essence Attributes', 'sodapop' ),
		'parent_item_colon'     => __( 'Parent Essence:', 'sodapop' ),
		'all_items'             => __( 'All Essences', 'sodapop' ),
		'add_new_item'          => __( 'Add New Essence', 'sodapop' ),
		'add_new'               => __( 'Add New', 'sodapop' ),
		'new_item'              => __( 'New Essence', 'sodapop' ),
		'edit_item'             => __( 'Edit Essence', 'sodapop' ),
		'update_item'           => __( 'Update Essence', 'sodapop' ),
		'view_item'             => __( 'View Essence', 'sodapop' ),
		'view_items'            => __( 'View Essences', 'sodapop' ),
		'search_items'          => __( 'Search Essence', 'sodapop' ),
		'not_found'             => __( 'Not found', 'sodapop' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'sodapop' ),
		'featured_image'        => __( 'Essence Image', 'sodapop' ),
		'set_featured_image'    => __( 'Set essence image', 'sodapop' ),
		'remove_featured_image' => __( 'Remove essence image', 'sodapop' ),
		'use_featured_image'    => __( 'Use as essence image', 'sodapop' ),
		'insert_into_item'      => __( 'Insert into essence', 'sodapop' ),
		'uploaded_to_this_item' => __( 'Uploaded to this essence', 'sodapop' ),
		'items_list'            => __( 'Essences list', 'sodapop' ),
		'items_list_navigation' => __( 'Essences list navigation', 'sodapop' ),
		'filter_items_list'     => __( 'Filter essences list', 'sodapop' ),
	);
	$args = array(
		'label'                 => __( 'Essence', 'sodapop' ),
		'description'           => __( 'Post Type Description', 'sodapop' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'essences',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'essence', $args );

}
add_action( 'init', 'essences', 0 );

/**
 * Accessories
 */
function accessories() {

	$labels = array(
		'name'                  => _x( 'Accessories', 'Post Type General Name', 'sodapop' ),
		'singular_name'         => _x( 'Accessory', 'Post Type Singular Name', 'sodapop' ),
		'menu_name'             => __( 'Accessories', 'sodapop' ),
		'name_admin_bar'        => __( 'Accessory', 'sodapop' ),
		'archives'              => __( 'Accessories Archives', 'sodapop' ),
		'attributes'            => __( 'Accessory Attributes', 'sodapop' ),
		'parent_item_colon'     => __( 'Parent Accessory:', 'sodapop' ),
		'all_items'             => __( 'All Accessories', 'sodapop' ),
		'add_new_item'          => __( 'Add New Accessory', 'sodapop' ),
		'add_new'               => __( 'Add New', 'sodapop' ),
		'new_item'              => __( 'New Accessory', 'sodapop' ),
		'edit_item'             => __( 'Edit Accessory', 'sodapop' ),
		'update_item'           => __( 'Update Accessory', 'sodapop' ),
		'view_item'             => __( 'View Accessory', 'sodapop' ),
		'view_items'            => __( 'View Accessories', 'sodapop' ),
		'search_items'          => __( 'Search Accessory', 'sodapop' ),
		'not_found'             => __( 'Not found', 'sodapop' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'sodapop' ),
		'featured_image'        => __( 'Accessory Image', 'sodapop' ),
		'set_featured_image'    => __( 'Set accessory image', 'sodapop' ),
		'remove_featured_image' => __( 'Remove accessory image', 'sodapop' ),
		'use_featured_image'    => __( 'Use as accessory image', 'sodapop' ),
		'insert_into_item'      => __( 'Insert into accessory', 'sodapop' ),
		'uploaded_to_this_item' => __( 'Uploaded to this accessory', 'sodapop' ),
		'items_list'            => __( 'Accessories list', 'sodapop' ),
		'items_list_navigation' => __( 'Accessories list navigation', 'sodapop' ),
		'filter_items_list'     => __( 'Filter accessories list', 'sodapop' ),
	);
	$args = array(
		'label'                 => __( 'Accessory', 'sodapop' ),
		'description'           => __( 'Post Type Description', 'sodapop' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', ),
		'taxonomies'            => array( 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'accessories',		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'accessory', $args );

}
add_action( 'init', 'accessories', 0 );

/**
 * Partners
 */
function partners() {

	$labels = array(
		'name'                  => _x( 'Partners', 'Post Type General Name', 'sodapop' ),
		'singular_name'         => _x( 'Partner', 'Post Type Singular Name', 'sodapop' ),
		'menu_name'             => __( 'Partners', 'sodapop' ),
		'name_admin_bar'        => __( 'Partner', 'sodapop' ),
		'archives'              => __( 'Partners Archives', 'sodapop' ),
		'attributes'            => __( 'Partner Attributes', 'sodapop' ),
		'parent_item_colon'     => __( 'Parent Partner:', 'sodapop' ),
		'all_items'             => __( 'All Partners', 'sodapop' ),
		'add_new_item'          => __( 'Add New Partner', 'sodapop' ),
		'add_new'               => __( 'Add New', 'sodapop' ),
		'new_item'              => __( 'New Partner', 'sodapop' ),
		'edit_item'             => __( 'Edit Partner', 'sodapop' ),
		'update_item'           => __( 'Update Partner', 'sodapop' ),
		'view_item'             => __( 'View Partner', 'sodapop' ),
		'view_items'            => __( 'View Partners', 'sodapop' ),
		'search_items'          => __( 'Search Partner', 'sodapop' ),
		'not_found'             => __( 'Not found', 'sodapop' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'sodapop' ),
		'featured_image'        => __( 'Partner Image', 'sodapop' ),
		'set_featured_image'    => __( 'Set partner image', 'sodapop' ),
		'remove_featured_image' => __( 'Remove partner image', 'sodapop' ),
		'use_featured_image'    => __( 'Use as partner image', 'sodapop' ),
		'insert_into_item'      => __( 'Insert into partner', 'sodapop' ),
		'uploaded_to_this_item' => __( 'Uploaded to this partner', 'sodapop' ),
		'items_list'            => __( 'Partners list', 'sodapop' ),
		'items_list_navigation' => __( 'Partners list navigation', 'sodapop' ),
		'filter_items_list'     => __( 'Filter partners list', 'sodapop' ),
	);
	$args = array(
		'label'                 => __( 'Partner', 'sodapop' ),
		'description'           => __( 'Post Type Description', 'sodapop' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'thumbnail', ),
		'taxonomies'            => array( 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'partner', $args );

}
add_action( 'init', 'partners', 0 );


/**
 * FAQ
 */
function faq() {

	$labels = array(
		'name'                  => _x( 'Questions', 'Post Type General Name', 'sodapop' ),
		'singular_name'         => _x( 'Question', 'Post Type Singular Name', 'sodapop' ),
		'menu_name'             => __( 'FAQ', 'sodapop' ),
		'name_admin_bar'        => __( 'Question', 'sodapop' ),
		'archives'              => __( 'Questions Archives', 'sodapop' ),
		'attributes'            => __( 'Question Attributes', 'sodapop' ),
		'parent_item_colon'     => __( 'Question Partner:', 'sodapop' ),
		'all_items'             => __( 'All Questions', 'sodapop' ),
		'add_new_item'          => __( 'Add New Question', 'sodapop' ),
		'add_new'               => __( 'Add New', 'sodapop' ),
		'new_item'              => __( 'New Question', 'sodapop' ),
		'edit_item'             => __( 'Edit Question', 'sodapop' ),
		'update_item'           => __( 'Update Question', 'sodapop' ),
		'view_item'             => __( 'View Question', 'sodapop' ),
		'view_items'            => __( 'View Questions', 'sodapop' ),
		'search_items'          => __( 'Search Question', 'sodapop' ),
		'not_found'             => __( 'Not found', 'sodapop' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'sodapop' ),
		'featured_image'        => __( 'Question Image', 'sodapop' ),
		'set_featured_image'    => __( 'Set question image', 'sodapop' ),
		'remove_featured_image' => __( 'Remove question image', 'sodapop' ),
		'use_featured_image'    => __( 'Use as question image', 'sodapop' ),
		'insert_into_item'      => __( 'Insert into question', 'sodapop' ),
		'uploaded_to_this_item' => __( 'Uploaded to this question', 'sodapop' ),
		'items_list'            => __( 'Questions list', 'sodapop' ),
		'items_list_navigation' => __( 'Questions list navigation', 'sodapop' ),
		'filter_items_list'     => __( 'Filter questions list', 'sodapop' ),
	);
	$args = array(
		'label'                 => __( 'Question', 'sodapop' ),
		'description'           => __( 'Post Type Description', 'sodapop' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', ),
		'taxonomies'            => array( 'category' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => false,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'question', $args );

}
add_action( 'init', 'faq', 0 );


/**
 * Press
 */
function press() {

	$labels = array(
		'name'                  => _x( 'Press Releases', 'Post Type General Name', 'sodapop' ),
		'singular_name'         => _x( 'Press Release', 'Post Type Singular Name', 'sodapop' ),
		'menu_name'             => __( 'Press', 'sodapop' ),
		'name_admin_bar'        => __( 'Press Release', 'sodapop' ),
		'archives'              => __( 'Press Archives', 'sodapop' ),
		'attributes'            => __( 'Press Release Attributes', 'sodapop' ),
		'parent_item_colon'     => __( 'Press Release Partner:', 'sodapop' ),
		'all_items'             => __( 'All Press Releases', 'sodapop' ),
		'add_new_item'          => __( 'Add New Press Release', 'sodapop' ),
		'add_new'               => __( 'Add New', 'sodapop' ),
		'new_item'              => __( 'New Press Release', 'sodapop' ),
		'edit_item'             => __( 'Edit Press Release', 'sodapop' ),
		'update_item'           => __( 'Update Press Release', 'sodapop' ),
		'view_item'             => __( 'View Press Release', 'sodapop' ),
		'view_items'            => __( 'View Press Releases', 'sodapop' ),
		'search_items'          => __( 'Search Press Release', 'sodapop' ),
		'not_found'             => __( 'Not found', 'sodapop' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'sodapop' ),
		'featured_image'        => __( 'Press Release Image', 'sodapop' ),
		'set_featured_image'    => __( 'Set press release image', 'sodapop' ),
		'remove_featured_image' => __( 'Remove press release image', 'sodapop' ),
		'use_featured_image'    => __( 'Use as press release image', 'sodapop' ),
		'insert_into_item'      => __( 'Insert into press release', 'sodapop' ),
		'uploaded_to_this_item' => __( 'Uploaded to this press release', 'sodapop' ),
		'items_list'            => __( 'Press list', 'sodapop' ),
		'items_list_navigation' => __( 'Press list navigation', 'sodapop' ),
		'filter_items_list'     => __( 'Filter press list', 'sodapop' ),
	);
	$args = array(
		'label'                 => __( 'Press Release', 'sodapop' ),
		'description'           => __( 'Post Type Description', 'sodapop' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'press-releases',
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'press-release', $args );

}
add_action( 'init', 'press', 0 );


/**
 * Bottle shirts
 */
function bottle_sleeves() {

	$labels = array(
		'name'                  => _x( 'Bottle Shirts', 'Post Type General Name', 'sodapop' ),
		'singular_name'         => _x( 'Bottle Shirt', 'Post Type Singular Name', 'sodapop' ),
		'menu_name'             => __( 'Bottle Shirts', 'sodapop' ),
		'name_admin_bar'        => __( 'Bottle Shirt', 'sodapop' ),
		'archives'              => __( 'Bottle Shirt Archives', 'sodapop' ),
		'attributes'            => __( 'Bottle Shirt Attributes', 'sodapop' ),
		'parent_item_colon'     => __( 'Parent Bottle Shirt:', 'sodapop' ),
		'all_items'             => __( 'All Bottle Shirts', 'sodapop' ),
		'add_new_item'          => __( 'Add New Bottle Shirt', 'sodapop' ),
		'add_new'               => __( 'Add New', 'sodapop' ),
		'new_item'              => __( 'New Bottle Shirt', 'sodapop' ),
		'edit_item'             => __( 'Edit Bottle Shirt', 'sodapop' ),
		'update_item'           => __( 'Update Bottle Shirt', 'sodapop' ),
		'view_item'             => __( 'View Bottle Shirt', 'sodapop' ),
		'view_items'            => __( 'View Bottle Shirts', 'sodapop' ),
		'search_items'          => __( 'Search Bottle Shirt', 'sodapop' ),
		'not_found'             => __( 'Not found', 'sodapop' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'sodapop' ),
		'featured_image'        => __( 'Bottle Shirt Image', 'sodapop' ),
		'set_featured_image'    => __( 'Set bottle shirt image', 'sodapop' ),
		'remove_featured_image' => __( 'Remove bottle shirt image', 'sodapop' ),
		'use_featured_image'    => __( 'Use as bottle shirt image', 'sodapop' ),
		'insert_into_item'      => __( 'Insert into bottle shirt', 'sodapop' ),
		'uploaded_to_this_item' => __( 'Uploaded to this bottle shirt', 'sodapop' ),
		'items_list'            => __( 'Bottle Shirts list', 'sodapop' ),
		'items_list_navigation' => __( 'Bottle Shirts list navigation', 'sodapop' ),
		'filter_items_list'     => __( 'Filter bottle shirts list', 'sodapop' ),
	);
	$args = array(
		'label'                 => __( 'Bottle Shirt', 'sodapop' ),
		'description'           => __( 'Post Type Description', 'sodapop' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail', ),
		'taxonomies'            => array( 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => 'bottle-sleeves',		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'bottle-sleeve', $args );

}
add_action( 'init', 'bottle_sleeves', 0 );


/**
 * Minify html markup
 */
class WP_HTML_Compression
{
    // Settings
    protected $compress_css = true;
    protected $compress_js = true;
    protected $info_comment = true;
    protected $remove_comments = true;

    // Variables
    protected $html;
    public function __construct($html)
    {
   	 if (!empty($html))
   	 {
   		 $this->parseHTML($html);
   	 }
    }
    public function __toString()
    {
   	 return $this->html;
    }
    protected function bottomComment($raw, $compressed)
    {
   	 $raw = strlen($raw);
   	 $compressed = strlen($compressed);
   	 
   	 $savings = ($raw-$compressed) / $raw * 100;
   	 
   	 $savings = round($savings, 2);
   	 
   	 return '<!--HTML compressed, size saved '.$savings.'%. From '.$raw.' bytes, now '.$compressed.' bytes-->';
    }
    protected function minifyHTML($html)
    {
   	 $pattern = '/<(?<script>script).*?<\/script\s*>|<(?<style>style).*?<\/style\s*>|<!(?<comment>--).*?-->|<(?<tag>[\/\w.:-]*)(?:".*?"|\'.*?\'|[^\'">]+)*>|(?<text>((<[^!\/\w.:-])?[^<]*)+)|/si';
   	 preg_match_all($pattern, $html, $matches, PREG_SET_ORDER);
   	 $overriding = false;
   	 $raw_tag = false;
   	 // Variable reused for output
   	 $html = '';
   	 foreach ($matches as $token)
   	 {
   		 $tag = (isset($token['tag'])) ? strtolower($token['tag']) : null;
   		 
   		 $content = $token[0];
   		 
   		 if (is_null($tag))
   		 {
   			 if ( !empty($token['script']) )
   			 {
   				 $strip = $this->compress_js;
   			 }
   			 else if ( !empty($token['style']) )
   			 {
   				 $strip = $this->compress_css;
   			 }
   			 else if ($content == '<!--wp-html-compression no compression-->')
   			 {
   				 $overriding = !$overriding;
   				 
   				 // Don't print the comment
   				 continue;
   			 }
   			 else if ($this->remove_comments)
   			 {
   				 if (!$overriding && $raw_tag != 'textarea')
   				 {
   					 // Remove any HTML comments, except MSIE conditional comments
   					 $content = preg_replace('/<!--(?!\s*(?:\[if [^\]]+]|<!|>))(?:(?!-->).)*-->/s', '', $content);
   				 }
   			 }
   		 }
   		 else
   		 {
   			 if ($tag == 'pre' || $tag == 'textarea')
   			 {
   				 $raw_tag = $tag;
   			 }
   			 else if ($tag == '/pre' || $tag == '/textarea')
   			 {
   				 $raw_tag = false;
   			 }
   			 else
   			 {
   				 if ($raw_tag || $overriding)
   				 {
   					 $strip = false;
   				 }
   				 else
   				 {
   					 $strip = true;
   					 
   					 // Remove any empty attributes, except:
   					 // action, alt, content, src
   					 $content = preg_replace('/(\s+)(\w++(?<!\baction|\balt|\bcontent|\bsrc)="")/', '$1', $content);
   					 
   					 // Remove any space before the end of self-closing XHTML tags
   					 // JavaScript excluded
   					 $content = str_replace(' />', '/>', $content);
   				 }
   			 }
   		 }
   		 
   		 if ($strip)
   		 {
   			 $content = $this->removeWhiteSpace($content);
   		 }
   		 
   		 $html .= $content;
   	 }
   	 
   	 return $html;
    }
   	 
    public function parseHTML($html)
    {
   	 $this->html = $this->minifyHTML($html);
   	 
   	 if ($this->info_comment)
   	 {
   		 $this->html .= "\n" . $this->bottomComment($html, $this->html);
   	 }
    }
    
    protected function removeWhiteSpace($str)
    {
   	 $str = str_replace("\t", ' ', $str);
   	 $str = str_replace("\n",  '', $str);
   	 $str = str_replace("\r",  '', $str);
   	 
   	 while (stristr($str, '  '))
   	 {
   		 $str = str_replace('  ', ' ', $str);
   	 }
   	 
   	 return $str;
    }
}

function wp_html_compression_finish($html)
{
    return new WP_HTML_Compression($html);
}

function wp_html_compression_start()
{
    ob_start('wp_html_compression_finish');
}
add_action('get_header', 'wp_html_compression_start');


/*	
 * Getting script tags
 */
// add_action( 'wp_print_scripts', 'print_scripts' );
// function print_scripts() {
// 	global $wp_scripts;
// 	foreach( $wp_scripts->queue as $handle ) :
// 		echo $handle . ' | ';
// 	endforeach;
// }

add_filter( 'script_loader_tag', 'defer_scripts', 10, 3 );
function defer_scripts( $tag, $handle, $src ) {

	// The handles of the enqueued scripts we want to defer
	$defer_scripts = array( 
		'jquery',
		'sodapop-bootstrap-js',
		'sodapop-modernizr-js',
		'sodapop-swiper-js',
		// 'sodapop-isotope-js',
		'sodapop-main-js',
		'sodapop-optimize-js',
	);

    if ( in_array( $handle, $defer_scripts ) ) {
        return '<script type="text/javascript" src="' . $src . '" defer="defer"></script>' . "\n";
    }
    
    return $tag;
}

