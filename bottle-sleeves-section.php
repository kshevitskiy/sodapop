<?php
/**
 * The template for displaying bottle sleeves section on homepage
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sodapop
 */

$sleeves_headline = get_field('sleeves_headline');
$sleeves_copy = get_field('sleeves_copy');
$sleeves_link = get_field('sleeves_link');
$sleeves_background_color = get_field('sleeves_background_color');
$sleeves_background_image = get_field('sleeves_background_image');
if( !empty( $sleeves_headline )  &&
	!empty( $sleeves_copy ) &&
    !empty( $sleeves_link ) && 
    !empty( $sleeves_background_image ) ) : ?>

    <section class="section bottles-section" id="bottle-shirts" style="background-color: <?php echo $sleeves_background_color; ?>">
        <div class="bottles-section__container container">
            <header class="section-header">
                <div class="text-center">                                            
                    <?php 
                    if ( is_main_site() ) {
                        echo '<span class="pretitle text-uppercase u-color-default">Zubehör</span>';
                    } else {
                        echo '<span class="pretitle text-uppercase u-color-default">Accessories</span>';
                    }
                    ?>
                    <h2 class="h4 u-color-default">
                        <?php echo $sleeves_headline; ?>
                    </h2>
                    <div class="copy copy--color-default">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
                                <?php echo $sleeves_copy; ?>  
                            </div>
                        </div>                        
                    </div>                    
                    <?php 
                    if ( is_main_site() ) {
                        echo '<a href="'. $sleeves_link .'" class="btn btn-more">Entdecken Sie mehr</a>';
                    } else {
                        echo '<a href="'. $sleeves_link .'" class="btn btn-more">Explore more</a>';                                   
                    }
                    ?>                     
                </div>
            </header>
        </div>

        <div class="section-container section-background">
            <img src="<?php echo $sleeves_background_image['url']; ?>" alt="<?php echo $sleeves_background_image['alt']; ?>" class="u-fade-out" />
        </div>            
    </section>    
<?php
endif;