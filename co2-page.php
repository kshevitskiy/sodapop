<?php
/**
 * Template Name: CO2 Refil
 *
 * The template for displaying pressroom page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sodapop
 */

get_header(); 

$bg = get_field('background_image');
$summary = get_field('summary');
$image = get_field('image');
?>

	<main id="main" class="site-main">

		<div class="product">			
    		<section class="product-section product-intro-section u-bg-gray-lightest" <?php if( !empty( $bg ) ) { echo 'style="background-image: url('. $bg .');"'; } ?>>
	    		<div class="container">
	    			<div class="row u-flexbox u-flex-align-center u-reverse-mobile">
	    				<div class="col-xs-12 col-sm-7 col-md-7">	    					
		    				<header class="product-headline">
		    					<?php the_title( '<h1 class="page-header__title">', '</h1>' ); ?>
		    				</header>
							<?php
							if( !empty( $summary ) ) : ?>
			    				<div class="product-summary">
			    					<div class="copy">
			    						<?php echo $summary; ?>	
			    					</div>
			    				</div>								
							<?php
							endif; ?>
	    				</div>
	    				<?php
	    				if ( has_post_thumbnail() ) : ?>
		    				<div class="col-xs-12 col-sm-5 col-md-5">	    					
			    				<figure class="product-image u-img-center">
			    					<?php the_post_thumbnail(); ?>
	                                <span class="one-for-all-badge">
	                                    <img src="<?php echo get_template_directory_uri(); ?>/img/one4all.png" alt="One for all" style="position: absolute; right: 30%; bottom: 5%; margin: auto; max-width: 20%;">
	                                </span>
			    				</figure>
		    				</div>
	    				<?php
	    				endif; ?>
	    			</div>
	    		</div>
    		</section>

            <section class="product-section">
                <div class="container">
                    <div class="row u-flexbox u-flex-align-center">
						<?php
						if( !empty( $image ) ) : ?>							
	                        <div class="col-xs-12 col-sm-4 col-md-4">
	                            <figure class="u-img-center">
	                                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
	                            </figure>	                            
	                        </div>		    				
						<?php
						endif; 

						if (have_posts()) : while (have_posts()) : the_post();
							if ( !empty( get_the_content() ) ) : ?>
	                        <div class="col-xs-12 col-sm-8 col-md-8">
	                            <div class="copy">
	                                <?php the_content(); ?>
	                            </div>
	                        </div>
							<?php
							endif;
						endwhile; endif; ?>
                    </div>
                </div>
            </section>    		
    	</div>  	

	</main><!-- #main -->

<?php
get_footer();