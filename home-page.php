<?php
/**
 * Template Name: Homepage
 *
 * The template for displaying homepage
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sodapop
 */

get_header(); ?>

	<main id="main" class="site-main">
		<?php
			get_template_part( 'slider' );
			get_template_part( 'units' );
			get_template_part( 'essences-section' );
			get_template_part( 'essences' );
			get_template_part( 'photo-section' );			
			get_template_part( 'accessories-section' );
			get_template_part( 'bottle-sleeves-section' );
			get_template_part( 'carbon-dioxide-section' );
			// get_template_part( 'contact-section' );
			// get_template_part( 'partners' );
			get_template_part( 'faq' );
		?>
	</main><!-- #main -->

<?php
get_footer();