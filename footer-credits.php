<?php
/**
 * The footer credits container
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sodapop
 */
?>

<div class="site-footer-credits">
    <div class="container">
        <div class="row sm flex">                        
            <div class="col-xs-12 col-sm-6 col-md-3">
                <span class="u-text-xs">©<?php echo date('Y'); ?> Copyright by MY SODA POP</span>
            </div>
            <div class="footer-social col-xs-12 col-sm-6 col-md-3">
                <?php 
                if ( is_main_site() ) {
                    echo '<span class="footer-social__label u-text-xs pretitle text-uppercase">Erreichen Sie uns unter</span>';
                } else {
                    echo '<span class="footer-social__label u-text-xs pretitle text-uppercase">Reach us at</span>';
                }
                ?>                                
                <?php
                    wp_nav_menu( array(
                        'container'       => 'ul',
                        'theme_location'  => 'menu-4',
                        'menu_id'         => 'social',
                        'menu_class'      => 'social',
                    ) );                                
                ?>                 
            </div>

            <div class="col-xs-12 col-sm-6 col-md-3 col-md-offset-3">
                <figure class="site-footer-logo">
                    <a href="<?php bloginfo( 'url' ); ?>">
                        <img src="<?php echo get_template_directory_uri(); ?>/img/logo-footer.png" alt="<?php bloginfo( 'name' ); ?>">
                    </a>                    
                </figure>
            </div>
        </div>
    </div>
</div>