<?php
/**
 * The template for displaying searchform
 *
 * @package sodapop
 */
?>

<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">                        
    <div class="inline-input">
        <div class="input-group form-group">
            <input type="search" class="form-control" placeholder="Search for" value="" name="s" title="Search for:" />
            <span class="input-group-addon">
                <button type="submit" class="inline-input__button search-submit">
                    <span class="sr-only">Search</span>
                </button>
            </span>
        </div>
    </div>                        
</form>