<?php
/**
 * All products container
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sodapop
 */
?>

<?php

$args = array(
    'post_type'              => array( 'unit', 'essence', 'accessory', 'bottle-sleeve' ),
    'post_status'            => array( 'publish' ),
    'posts_per_page'         => '100',
    'order'                  => 'ASC',
);

// The Query
$query = new WP_Query( $args );
$url = get_template_directory_uri();
?>

<?php
// The Loop
if ( $query->have_posts() ) { ?>    

    <aside id="all-products" class="all-products modal show-modal">
        <div class="all-products__inner">            
            <header class="all-products__header modal-header">
                <div class="btn-group iso-filters hidden-xs">
                    <button data-sort-by="*" class="btn iso-filter btn-default btn-sm">All</button>
                    <button data-sort-by=".unit" class="btn iso-filter btn-default btn-sm">
                        <span class="u-img-center">
                            <img src="<?php echo $url; ?>/img/icons/sharon.png" alt="Units" class="cat-img">
                        </span>
                        <span>Units</span>
                    </button>
                    <button data-sort-by=".essence" class="btn iso-filter btn-default btn-sm">                    
                        <span class="u-img-center">
                            <img src="<?php echo $url; ?>/img/icons/essence.svg" alt="Essences" class="cat-img">
                        </span>                    
                        <span>Essences</span>                    
                    </button>                    
                    <button data-sort-by=".accessory" class="btn iso-filter btn-default btn-sm">
                        <span class="u-img-center">
                            <img src="<?php echo $url; ?>/img/icons/bottle.png" alt="Accessories" class="cat-img">
                        </span>                    
                        <span>Bottles</span>
                    </button>                                    
                    <button data-sort-by=".bottle-sleeve" class="btn iso-filter btn-default btn-sm">
                        <span class="u-img-center">
                            <img src="<?php echo $url; ?>/img/icons/bottle.png" alt="Bottle Sleeves" class="cat-img">
                        </span>                    
                        <span>Bottle Shirts</span>
                    </button>
                </div>

                <div class="select visible-xs">
                    <div class="select__inner">                    
                        <label for="filter-products" class="select__label u-text-sm u-color-gray-lighter">Filter products:</label>
                        <select class="iso-filters" id="filter-products">                            
                            <option value="*" data-sort-by="*">All</option>                            
                            <option value=".unit">Units</option>
                            <option value=".essence">Essences</option>
                            <option value=".accessory">Bottles</option>
                            <option value=".bottle-sleeve">Bottle Shirts</option>                            
                        </select>
                    </div>
                </div>
            </header>

            <div class="modal-body">
                <ul class="products iso-items">

                <?php
                while ( $query->have_posts() ) { 
                    $query->the_post();
                    $id = get_field('product_id');
                    $title = get_the_title();
                    $link = get_the_permalink();
                    $post_type = get_post_type_object( get_post_type($post) );
                        if ( has_post_thumbnail() ) { ?>
                            <li <?php post_class('product-item slide iso-item'); ?>>
                                <div class="product-item__inner">
                                    <div class="row u-flexbox u-no-wrap">
                                        <figure class="product-item__image col-xs-3 col-sm-4 u-img-center">
                                            <div class="u-fade-out">
                                                <?php the_post_thumbnail('product-grid'); ?>
                                            </div>
                                        </figure>                                        
                                        <header class="product-item__header col-xs-9 col-sm-8">
                                            <?php
                                            if( !empty( $id ) ) :
                                                echo '<div class="roduct-item__id u-text-sm u-color-gray-lighter">'. $id .'</div>';
                                            endif;
                                            ?>
                                            <p class="u-text-xs u-color-gray-lighter">
                                                <?php echo $post_type->label ; ?>
                                            </p>                                        
                                            <h3 class="product-item__title h5">
                                                <?php echo $title; ?>
                                            </h3>    
                                            <?php 
                                            if ( 'bottle-sleeve' !== get_post_type() ) : 
                                                echo '<div class="copy u-text-sm">'. content(10) .'</div>';
                                                if ( is_main_site() ) {
                                                    echo '<a href="'. $link .'" class="btn btn-link">Entdecken Sie mehr</a>';
                                                } else {
                                                    echo '<a href="'. $link .'" class="btn btn-link">Explore more</a>';
                                                }                                                
                                            endif; ?>
                                        </header>
                                    </div>
                                </div>
                            </li>
                        <?php
                        }
                }
                ?>

                </ul>
            </div>
        </div>
    </aside>

<?php
} else {
    // No posts found
}

// Restore original Post Data
wp_reset_postdata(); 

                                                                              