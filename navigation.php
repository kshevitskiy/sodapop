<?php
/**
 * The template for displaying navigation
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sodapop
 */
?>

<aside class="site-menu">
    <div class="site-menu__inner">        
        <?php 
            if ( is_main_site() ) {
                echo '<h4 class="site-menu__title u-text-sm text-uppercase">Wo wollen Sie hin?</h4>';
            } else {
                echo '<h4 class="site-menu__title u-text-sm text-uppercase">Where you want to go?</h4>';
            }

            wp_nav_menu( array(
                'container'       => 'nav',
                'container_id'    => 'site-navigation',
                'container_class' => 'nav',
                'theme_location'  => 'menu-1',
                'menu_id'         => 'primary-menu',
            ) );                                

            get_template_part( 'navigation-widgets' ) 
        ?>
    </div>
</aside> 