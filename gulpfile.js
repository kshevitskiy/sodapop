const gulp = require('gulp');
const gulpImports = require('gulp-imports');
const importCss = require('gulp-import-css');
const jslint = require('gulp-eslint');
const browserSync = require('browser-sync');
const gulpLoadPlugins = require('gulp-load-plugins');
const $ = gulpLoadPlugins();
const reload = browserSync.reload;
const changed = require('gulp-changed');
const source = require('vinyl-source-stream');
const imagemin = require('gulp-imagemin');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const browserify = require('gulp-browserify');
const w3cValidation = require('gulp-w3c-html-validation');
const gutil = require('gulp-util');
const gulpif = require('gulp-if');

const serve = 'serve';

const paths = {
    dist: {
        dir:      'dist',
        css:      'dist/css',
        html:     'dist/**/*.html',
        img:      'dist/img',
        video:    'dist/video',
        json:     'dist/json',
        scripts:  'dist/scripts',
        favicons: 'dist/favicons',
        fonts:    'dist/fonts'
    },

    wp: {
        dir: '/',
        js:  'js',
        css: 'css',
        img: 'img'
    },

    src:     'app',
    libs:    'app/libs',
    html:    'app/*.html',
    images:  'app/img/**/*.{png,jpg,gif,svg}',
    favicon: 'app/img/favicon.png',
    videos:  'app/video/*.{mp4,webm}',
    json:    'app/json/*.json',
    scss:    'app/scss/style.scss',
    scripts: 'app/scripts/main.js',
    fonts:   'app/fonts/*.ttf'
};

const config = {
    sourceMaps: !gutil.env.production,

    makeStyles: function(source, destination, isMinify) {
        return gulp.src(source)
            .pipe($.plumber())
            .pipe(gulpif(config.sourceMaps, $.sourcemaps.init()))
            .pipe($.sass.sync({
                outputStyle: 'expanded',
                precision: 10,
                includePaths: ['.']
            }).on('error', $.sass.logError))
            .pipe(isMinify ? $.cssnano({ zindex: false }) : gutil.noop())
            .pipe($.autoprefixer({browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']}))
            .pipe(gulpif(config.sourceMaps, $.sourcemaps.write()))
            .pipe(gulp.dest(destination))
            .pipe(reload({stream: true}));
    },

    copy: function(source, destination) {
        return gulp.src(source)
            .pipe(gulp.dest(destination));
    }
};


gulp.task('styles', function() {
    config.makeStyles(paths.scss, paths.dist.css, true);
});


gulp.task('scripts', function() {
  gulp.src(paths.scripts)
        .pipe(gulpImports())
        .pipe(browserify({ insertGlobals: true, debug: !gulp.env.production }))
        .pipe(uglify())
        .pipe(gulp.dest(paths.dist.scripts));
  reload();
});


gulp.task('images', function() {
    gulp.src(paths.images)
    .pipe(changed(paths.dist.img))
    .pipe(imagemin({
        optimizationLevel: 3,
        progessive: true,
        interlaced: true
    }))
    .pipe(gulp.dest(paths.dist.img));
});


gulp.task('html', function() {
    gulp.src(paths.html)
        .pipe(changed(paths.dist.dir));
        //.pipe(w3cValidation());
});


gulp.task('copy', function() {
    config.copy(paths.dist.css + '/**/*', paths.wp.css);
    config.copy(paths.dist.scripts + '/**/*', paths.wp.js);
    config.copy(paths.dist.img + '/**/*', paths.wp.img);
});


/** 
 * Gulp tasks
 *
 * Utilities:
 * Build the project without sourcemaps: 'gulp --production' 
 */
gulp.task(serve, ['styles','scripts','images','html', 'copy'], function() {
    browserSync({
        notify: false,
        port: 9000,
        server: {
          baseDir: [paths.dist.dir, paths.src]
        }
    });

    gulp.watch([paths.src + '/scripts/**/*.js'],['scripts', 'copy']);
    gulp.watch([paths.src + '/scss/**/*.scss'], ['styles', 'copy']);
    gulp.watch([paths.images], ['images', 'copy']);
    gulp.watch([paths.html], ['html']);

    gulp.watch([paths.src + '/*'], reload);
});

gulp.task('default', ['styles','scripts','images','html', 'serve']);
