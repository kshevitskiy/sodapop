<?php
/**
 * The template for displaying essences on homepage
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sodapop
 */
?>

<?php

$args = array(
	'post_type'              => array( 'essence' ),
	'post_status'            => array( 'publish' ),
	'posts_per_page'         => '8',
);

// The Query
$query = new WP_Query( $args );
$q = array();
?>

<?php
// The Loop
if ( $query->have_posts() ) { ?>	

	<section class="section featured-essences">       		
				<?php
				while ( $query->have_posts() ) { 
					$query->the_post();

					if ( has_post_thumbnail() ) {                        

						// Categories
						$categories = get_the_category();	
						foreach ( $categories as $key=>$category ) {
							$id = $category->term_id;
							$name = $category->name;
							$image = '';
			                if( ( $image = category_image_src( array('term_id'=>$id), false ) ) != null ) {
								$image = '<span class="u-img-center"><img src="'. $image .'" alt="'. $name .'" class="cat-img" /></span>';
			                }
						    $button = '<button data-sort-by=".cat-' . $id . '" class="btn iso-filter btn-sm">
						    				'. $image .'
						    				'. $name .'
						    			</button>';
						    $catID = $id;
						}

						// Essence						
						$title 		 = get_the_title();
						$subtitle 	 = get_field('subtitle');
						$has_subtitle = '';
						if ( !empty( $subtitle ) ) {
							$short_subtitle = strip_tags($subtitle);
							$has_subtitle = '<div class="essence__summary">'. substr($short_subtitle, 0, 45) .'...</div>';
						}


						$link 		 = get_the_permalink();
						$lang		 = '';
						if ( is_main_site() ) {
							$lang = '<a href="'. $link .'" class="essence__details btn btn-more">Entdecken Sie mehr</a>';
						} else {
							$lang = '<a href="'. $link .'" class="essence__details btn btn-more">Explore more</a>';
						}

						$badge 		 = get_field('badge'); 	
						$id  		 = get_field('product_id');
						$has_id  = '';
                        if( !empty( $id ) ) {
                        	$has_id  = '<div class="product__id u-color-default u-text-xs copy">'. $id .'</div>';
                        }
						$thumb_size  = '';											
						$thumb 		 = get_the_post_thumbnail($thumb_size);						
                    	if ( wp_is_mobile() ) {
                    		$thumb_size = 'essence-thumb-mobile';
                    	} else {
                    		$thumb_size = 'essence-thumb';
                    	}
                    	$has_badge	 = '';
						if ( !empty( $badge ) ) {
							$has_badge = '<span class="essence__badge badge"><img src="'. $badge['url'] .'" alt="'. $badge['alt'] .'" /></span>';
						}
						$has_cat	 = '';
						if( has_category(array($category->name)) ) {
							$has_cat = 'cat-'. $catID .'';
						}
						$bg_color 	 = get_field('background_color');

						$essence 	 = '<li class="essence swiper-slide iso-item col-xs-12 col-sm-6 col-md-4 '. $has_cat .'" style="background-color: '. $bg_color .'">
			                            <div class="row sm u-flexbox">
			                                <figure class="essence__image u-img-center col-xs-5 col-sm-4">
			                                	'. $thumb .'
			                                    '. $has_badge .'
			                                </figure>	                                
			                                <div class="essence__content col-xs-7 col-sm-8">			                                	
			                                	'. $has_id .'
			                                    <h4 class="essence__title">'. $title .'</h4>
			                                    '. $has_subtitle .'
			                                    '. $lang .'
			                                </div>
			                            </div>
			                        </li>';

						// Array with the category names and essences
						$q[$button][] = $essence;
					}
				}

				// Restore original Post Data
				wp_reset_postdata(); ?>

                <div class="filters hidden-xs hide">
                    <div class="text-center">                    
                        <div class="btn-group iso-essence-filters" data-toggle="btn-group">
                        	<button class="btn iso-filter btn-sm active" data-sort-by="*">All</button>
							<?php 						
							    foreach ($q as $key => $values) {
							    	echo $key;
							    } 
						    ?>
						</div>
					</div>
				</div>


				<div class="container-fluid u-no-padding featured-essences-bg">
			        <header class="section-header">
			            <div class="container">
			                <div class="text-center">
			                    <h2 class="u-font-artistic-primary u-color-default hide">#pop up your water</h2>
			                    <figure class="section-header__image">                        
			                        <img src="<?php echo get_template_directory_uri(); ?>/img/puyw.png" alt="#pop up your water">
			                    </figure>                        
			                </div>
			            </div>
			        </header>			       								
			        <div class="container">
						<div class="essences slider slider-mobile">
			                <ul class="swiper-wrapper iso-essences">
							    <?php							    
								    foreach ($q as $key=>$values) {							    	
							            foreach ($values as $value) {
							                echo $value;
							            }
								    }
								?>
			                    <li class="essence essence-compare iso-item swiper-slide u-bg-secondary col-xs-12 col-sm-6 col-md-4 hide">
			                        <div class="text-center">                                        
			                            <a href="#" class="link-more link-more--default">Compare</a>
			                        </div>
			                    </li>					
							</ul>
						</div>
			        </div>
				</div>
	</section>

<?php
} else {
	// No posts found
}

// Restore original Post Data
wp_reset_postdata();
      
    
