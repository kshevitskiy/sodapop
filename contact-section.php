<?php
/**
 * The template for displaying contact section on homepage
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sodapop
 */

$contact_pre_title = get_field('contact_pre_title');
$contact_headline = get_field('contact_headline');
$contact_copy = get_field('contact_copy');
// $contact_map = get_field('contact_map');
if( !empty( $contact_pre_title ) && 
	!empty( $contact_headline )  &&
    !empty( $contact_copy ) ) : ?>

    <section class="section map-section u-bg-primary">
        <div class="container">
            <div class="row sm">
                <div class="col-xs-12 col-md-4 col-md-offset-2">
                    <span class="pretitle text-uppercase u-color-default">
                        <?php echo $contact_pre_title; ?>
                    </span>
                    <h2 class="h4 u-color-default">
                        <?php echo $contact_headline; ?>
                    </h2>                        
                </div>
            </div>
            <div class="row sm flex">
                <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-2">
                    <div class="copy copy--color-default">
                        <?php echo $contact_copy; ?>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 hide">
                    <?php
                        get_search_form();
                    ?>                                    
                </div>
            </div>
        </div>
        <?php 
        if ( !empty( $contact_map ) ) :
            echo    '<div class="container">
                        <div class="row">                    
                            <div class="col-xs-12 col-md-9">
                                <div class="map">
                                    <div class="google-maps">
                                        '. $contact_map .'
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>';
        endif; ?>

    </section>        
<?php
endif;