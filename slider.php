<?php
/**
 * The template for displaying slider on homepage
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sodapop
 */
?>

<?php

$args = array(
	'post_type'              => array( 'slide' ),
	'post_status'            => array( 'publish' ),
	'posts_per_page'         => '20',
);

// The Query
$query = new WP_Query( $args );
?>

<?php
// The Loop
if ( $query->have_posts() ) { ?>	

	<div class="banners-slider slider" style="visibility: hidden">
		<div class="swiper-wrapper">

			<?php
			while ( $query->have_posts() ) { 				
				$query->the_post();

				$has_link  = get_field('slide_button');
				$set_link  = get_field('set_custom_link');
				$link 	   = '';
				if ( $set_link ) {
					$link = get_field('custom_slide_link');
				} else {
					$link = get_field('slide_link');
				}

				$image = '';
				$image_data = '';
				$mobile_image = get_field('slide_mobile_image');
				if( wp_is_mobile() ) {
					if( $mobile_image ) {
						$image = $mobile_image;
					} else {
						$image = get_the_post_thumbnail_url( $post_id, 'full', array( 'class' => 'banner__bg' ) );
						$image_data = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
					}					
				} else {
					$image = get_the_post_thumbnail_url( $post_id, 'full', array( 'class' => 'banner__bg' ) );
					$image_data = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
				}

				$image_width = $image_data[1]; // thumbnail's width
				$image_height = $image_data[2]; // thumbnail's height				

				if ( has_post_thumbnail() ) {
					if( $has_link ) {
						echo 	'<a href="'. $link .'" class="swiper-slide banner to u-img-center">
									<img src="'. $image .'" width="'. $image_width .'" height="'. $image_height .'">
								</a>';

					} else {
						echo 	'<div class="swiper-slide banner to u-img-center">
									<img src="'. $image .'" width="'. $image_width .'" height="'. $image_height .'">
						      	</div>';
					}
				}
			}
			?>

		</div>
		<div class="pagination pagination--default"></div>
	</div>

<?php
} else {
	// No posts found
}

// Restore original Post Data
wp_reset_postdata();
      
    
