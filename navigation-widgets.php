<?php
/**
 * The navigation widget container
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sodapop
 */

if ( ! is_active_sidebar( 'sidebar-3' ) ) {
	return;
}
?>

<div class="site-menu-contacts">
	<?php dynamic_sidebar( 'sidebar-3' ); ?>
</div>
