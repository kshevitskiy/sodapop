<?php
/**
 * The template for displaying units on homepage
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sodapop
 */
?>

<?php

$args = array(
	'post_type'              => array( 'unit' ),
	'post_status'            => array( 'publish' ),
	'posts_per_page'         => '10',
);

// The Query
$query = new WP_Query( $args );
?>

<?php
// The Loop
if ( $query->have_posts() ) { ?>	

	<div class="section" id="sodamaker">
		<div class="container-fluid">
			<ul class="units row">

			<?php
			while ( $query->have_posts() ) { 
				$query->the_post();
					if ( has_post_thumbnail() ) {
						$bg = get_field('background_color');
                    	$font = get_field('font_style'); 
                    	$color = get_field('color'); 
                    	$subtitle = get_field('subtitle'); 
                    	$tooltip_headline = get_field('tooltip_headline'); 
                    	$tooltip_copy = get_field('tooltip_copy');
                    	$package_image = get_field('package_image'); ?>
	                    <li class="unit sharon col-xs-12 col-sm-6 col-md-6" style="background-color: <?php echo $bg; ?>">
	                        <header class="unit__header">
	                            <span class="unit__pretitle" style="color: <?php echo $color; ?>">Meet</span>
	                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">	                            		                            
		                            <h2 class="unit__title <?php echo $font; ?>" style="color: <?php echo $color; ?>">	                 	
		                            	<?php the_title(); ?>
		                            </h2>
		                        </a>
	                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="btn btn-bubbles btn-bubbles--sm hidden-xs">
									<?php 
									if ( is_main_site() ) {
										echo '<span class="btn-bubbles-text"><span>Erfahren</span><span>Sie</span><span>mehr</span></span>';
									} else {
										echo '<span class="btn-bubbles-text"><span>Find</span><span>Out</span><span>More</span></span>';									
									}
									?>
	                            </a>
	                        </header>
	                        <figure class="unit__photo u-img-center">
	                            <?php the_post_thumbnail('unit-thumb'); 
                                if( !empty( $tooltip_headline ) && !empty( $tooltip_copy ) ) : ?>
		                            <div class="unit-link" style="top: 15%; left: 48%">		                                
										<?php 
										if ( is_main_site() ) {
											echo '<span class="sr-only">Entdecken Sie mehr</span>';
										} else {
											echo '<span class="sr-only">Explore more</span>';									
										}
										?>		                                
		                                <div class="unit-link-tt">
		                                    <span class="unit-link-tt__header h5">
		                                    	<?php echo $tooltip_headline; ?>
		                                    </span>
		                                    <span class="unit-link-tt__body">
		                                    	<?php echo $tooltip_copy; ?>		                                    
		                                	</span>
		                                </div>	                                
		                            </div>
	                            <?php endif; ?>
	                        </figure>
	                        <?php
	                        if( !empty( $subtitle ) ): ?>
		                        <footer class="unit__footer">
		                        	<div class="unit__footer-copy">		                        		
		                            	<p><?php echo $subtitle; ?></p>
		                        	</div>
		                        	<?php
		                        		if ( !empty( $package_image ) ) {
		                        			echo 	'<figure class="unit__package">
		                        						<img src="'. $package_image['url'] .'" alt="'. $package_image['alt'] .'" />
		                        					</figure>';
		                        		}
		                        	?>
		                        </footer>
	                    	<?php endif; ?>
	                    </li>				        
					<?php
					}
			}
			?>

			</ul>
		</div>
	</div>

<?php
} else {
	// No posts found
}

// Restore original Post Data
wp_reset_postdata();
      
    
