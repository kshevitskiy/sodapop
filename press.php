<?php
/**
 * The template for displaying press release posts on pressroom page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sodapop
 */
?>

<?php

$args = array(
	'post_type'              => array( 'press-release' ),
	'post_status'            => array( 'publish' ),
	'posts_per_page'         => '30',
);

// The Query
$query = new WP_Query( $args );
?>

<?php
// The Loop
if ( $query->have_posts() ) { ?>	

	<div class="press-section u-bg-gray-lightest">
        <div class="container"> 
    		<ul class="cards press-cards row u-flexbox">

				<?php
				while ( $query->have_posts() ) { 
					$query->the_post();
					$summary = get_field('summary');
						if ( has_post_thumbnail() ) { ?>

	                        <li class="card press-card col-xs-12 col-sm-6 col-md-4">
                        		<div class="card__inner">                                        
                                    <figure class="press-card__image">
                                        <div class="badge press-card__date">
                                            <span>
                                            	<?php echo get_the_date('d.m.Y'); ?>
                                            </span>
                                        </div>
                                        <?php the_post_thumbnail( 'press-thumb' ); ?>
                                    </figure>
                                    <div class="press-card__content">
                                        <h3 class="press-card__title h5">
                                        	<?php the_title(); ?>
                                        </h3>
                                        <div>                                        	
                                        	<?php echo content(15); ?>
                                        </div>                                       
                                    </div>
									<?php 
									if ( is_main_site() ) {
										echo '<a href="'. get_the_permalink() .'" class="btn btn-primary btn-more press-card__btn">Entdecken Sie mehr</a>';
									} else {
										echo '<a href="'. get_the_permalink() .'" class="btn btn-primary btn-more press-card__btn">Explore more</a>';
									}
									?>
                                </div>
	                        </li>							
						<?php
						}
				}
				?>

			</ul>
		</div>
	</div>

<?php
} else {
	// No posts found
}

// Restore original Post Data
wp_reset_postdata();
      
    
