<?php
/**
 * The footer video modal
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sodapop
 */
?>

<div class="modal modal-video fade" id="video" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"></button>
            </div>
            <div class="modal-body">
            	<div class="video-wrapper"></div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>