<?php
/**
 * The header select language dropdown
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sodapop
 */
?>

<div class="dropdown select-language">  
  	<?php
  		if ( has_nav_menu( 'menu-5' ) ) {

		  	if ( is_main_site() ) {
				echo '<button class="lang-btn dropdown-toggle" type="button" data-toggle="dropdown">DE</button>';
		  	} else {
		  		echo '<button class="lang-btn dropdown-toggle" type="button" data-toggle="dropdown">EN</button>';
		  	}		

	        wp_nav_menu( array(
	            'container'       => 'ul',
	            'container_id'    => 'lang',
	            'container_class' => 'dropdown-menu',
	            'theme_location'  => 'menu-5',
	            'menu_id'         => 'change-lang',
	            'menu_class'	  => 'dropdown-menu'
	        ) );
		}
    ?>
</div>

