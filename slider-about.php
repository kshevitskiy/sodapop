<?php
/**
 * The template for displaying slider on about page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sodapop
 */
?>

<?php

$args = array(
	'post_type'              => array( 'slide-about' ),
	'post_status'            => array( 'publish' ),
	'posts_per_page'         => '20',
);

// The Query
$query = new WP_Query( $args );
?>

<?php
// The Loop
if ( $query->have_posts() ) { ?>	

	<div class="about-slider slider" style="visibility: hidden">
		<div class="swiper-wrapper">

			<?php
			while ( $query->have_posts() ) { 				
				$query->the_post();

				$image = get_the_post_thumbnail_url( $post_id, 'large', array( 'class' => 'about-slide-image' ) );
				$image_data = wp_get_attachment_image_src( get_post_thumbnail_id(), 'large' );

				$image_width = $image_data[1]; // thumbnail's width
				$image_height = $image_data[2]; // thumbnail's height				

				if ( has_post_thumbnail() ) {
					echo '<div class="swiper-slide about-slide"><img src="'. $image .'" width="'. $image_width .'" height="'. $image_height .'"></div>';
				}
			}
			?>

		</div>
	</div>

<?php
} else {
	// No posts found
	get_template_part( 'page-header' );
}

// Restore original Post Data
wp_reset_postdata();