<?php
/**
 * The footer widgets container
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sodapop
 */

if ( ! is_active_sidebar( 'sidebar-2' ) ) {
	return;
}

$placeholder = '';
if ( is_main_site() ) {
	$placeholder = 'Ihre Email-Adresse';
} else {
	$placeholder = 'Type in your e-mail';
}

?>

<div class="site-footer-widgets">
	<div class="container">		
		<div class="row">		
			<div class="col-xs-12 col-sm-12 col-md-8">
				<div class="row sm">
					<?php dynamic_sidebar( 'sidebar-2' ); ?>
				</div>
			</div>

			<div class="col-xs-12 col-sm-12 col-md-4">
				<div class="row sm">
	                <section class="widget col-xs-12 col-sm-12 col-md-12">
	                    <h5 class="widget__title text-uppercase">Newsletter</h5>
	                    <div class="widget-newsletter">
	                        <div class="widget-newsletter__info">
								<?php 
								if ( is_main_site() ) {
									echo '<p>Bleiben Sie immer auf dem Laufenden und erhalten Sie Informationen über Aktionen und neue Produkte.<br>Für Newsletter anmelden</p>';
								} else {
									echo '<p>Stay in touch and get the newest info about our new essences and promotions.<br>Sign up</p>';
								}
								?>
	                        </div>
	                        <div class="inline-input">
								<form action="https://mysodaopop.us17.list-manage.com/subscribe/post?u=be342ac0e7f1a8b29e77d33b7&amp;id=b0700dfbe4" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
									<div class="input-group form-group">										
										<input type="email" value="" name="EMAIL" class="form-control" id="mce-EMAIL" placeholder="<?php echo $placeholder; ?>">
										<div id="mce-responses" class="clear">
											<div class="response" id="mce-error-response" style="display:none"></div>
											<div class="response" id="mce-success-response" style="display:none"></div>
										</div>
										<!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
								    	<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_be342ac0e7f1a8b29e77d33b7_b0700dfbe4" tabindex="-1" value=""></div>

		                                <span class="input-group-addon">
		                                    <button type="submit" class="inline-input__button" id="mc-embedded-subscribe">
												<?php 
												if ( is_main_site() ) {
													echo '<span class="sr-only">Für Newsletter anmelden</span>';
												} else {
													echo '<span class="sr-only">Sign up</span>';
												}
												?>	                                    		                                        
		                                    </button>  
		                                </span>
									</div>
								</form>                       
	                        </div>                            
	                    </div>
	                </section>
				</div>
			</div>
		</div>
	</div>
</div>
