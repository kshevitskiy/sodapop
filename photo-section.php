<?php
/**
 * The template for displaying photo section on homepage
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sodapop
 */

$photo = get_field('photo_section');
if( !empty( $photo ) ) : ?>

	<div class="section section-photo">
	    <div class="container"> 
	        <figure>
	            <img src="<?php echo $photo['url']; ?>" alt="<?php echo $photo['alt']; ?>" />
	        </figure>               
	    </div>
	</div>

<?php
endif; ?>