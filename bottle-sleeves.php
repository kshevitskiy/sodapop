<?php
/**
 * The template for displaying bottle sleeves
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sodapop
 */
?>

<?php

$args = array(
	'post_type'              => array( 'bottle-sleeve' ),
	'post_status'            => array( 'publish' ),
	'posts_per_page'         => '20',
);

// The Query
$query = new WP_Query( $args );
?>

<?php
// The Loop
if ( $query->have_posts() ) { ?>	

	<div class="page-section u-no-padding-top">
	    <div class="container">
	        <ul class="products-grid">				

			<?php
			while ( $query->have_posts() ) { 
				$query->the_post();
					if ( has_post_thumbnail() ) {
						$product_id = get_field('product_id');
						$bg_color = get_field('background_color');
						$second_image = get_field('image'); ?>

                        <li class="product">
                            <figure class="product__image u-img-center"
							    <?php
							    if( !empty( $bg_color ) ) :
							        echo 'style="
							        		background-color: '. $bg_color .'; 
							        	"';
							    endif;
							    ?>							    
                            >
		                       	<?php
		                        if( !empty( $second_image ) ) {
			                        echo '<div class="primary-image">'. get_the_post_thumbnail() .'</div>';
			                        echo '<div class="secondary-image">
			                        	<img src="'. $second_image['url'] .'" alt="'. $second_image['alt'] .'">
			                        </div>';                                
                                } else {
                                	the_post_thumbnail();
                                } ?>
                            </figure>
                            <header class="product__header">
                                <h3 class="product__title h6">
                                	<?php the_title(); ?>
                                </h3>
		                        <?php
		                        if( !empty( $product_id ) ): ?>
	                                <span class="product__id">
	                                	<?php echo $product_id; ?>
	                                </span>
		                    	<?php 
		                    	endif; ?>                                
                            </header>
                        </li>	                    				        
					<?php
					}
			}
			?>

			</ul>
		</div>
	</div>


<?php
} else {
	// No posts found
}

// Restore original Post Data
wp_reset_postdata();
      