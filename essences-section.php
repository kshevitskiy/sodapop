<?php
/**
 * The template for displaying essences section on homepage
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sodapop
 */

$essences_image = get_field('essences_image');
$essences_pre_title = get_field('essences_pre_title');
$essences_headline = get_field('essences_headline');
$essences_copy_large = get_field('essences_copy_large');
$essences_copy = get_field('essences_copy');
if( !empty( $essences_pre_title ) && 
	!empty( $essences_headline )  &&
	!empty( $essences_copy_large ) && 
	!empty( $essences_copy ) ) : ?>
    <section class="section essences-section" id="essences">
        <div class="container">
            <?php            
            if ( !empty( $essences_image ) ) : ?>
            <div class="row">
                <div class="col-xs-12 col-md-6 col-md-offset-3">
                    <figure class="u-img-center">
                        <img src="<?php echo $essences_image['url']; ?>" alt="<?php echo $essences_image['alt']; ?>" />
                    </figure>
                    <br>
                </div>
            </div>
            <?php
            endif; ?>
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2">                    
                    <span class="pretitle u-text-sm text-uppercase">
                    	<?php echo $essences_pre_title; ?>
                    </span>
                    <h2 class="h4">
                    	<?php echo $essences_headline; ?>
                    </h2>
                    <div class="copy u-copy-lg">
                    	<?php echo $essences_copy_large; ?>
                    </div>
                    <div class="copy">
                    	<?php echo $essences_copy; ?>
                    </div>                        
                </div>
            </div>           
        </div>
    </section>				
<?php
endif;
    
