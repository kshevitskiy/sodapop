<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sodapop
 */

get_header(); ?>

		<main id="main" class="site-main">			
			<?php
			if ( have_posts() ) : ?>

				<header class="page-header">
					<div class="text-center">
						<?php
							the_archive_title( '<h1 class="page-title text-uppercase">', '</h1>' );
							the_archive_description( '<div class="archive-description">', '</div>' );
						?>
					</div>
				</header><!-- .page-header -->								
			
				<?php
				$args = array(
					'post_type'              => array( 'essence' ),
					'post_status'            => array( 'publish' ),
					'posts_per_page'         => '-1',
				);

				// The Query
				$query = new WP_Query( $args );
				$q = array();

				// The Loop
				if ( $query->have_posts() ) :
								
					while ( $query->have_posts() ) { 
						$query->the_post();

							// Categories
							$categories = get_the_category();	
							foreach ( $categories as $key=>$category ) {
								$id = $category->term_id;
								$name = $category->name;
								$image = '';
				                if( ( $image = category_image_src( array('term_id'=>$id), false ) ) != null ) {
									$image = '<figure class="u-img-center"><img src="'. $image .'" alt="'. $name .'" class="cat-img" /></figure>';
				                }
							    $button = '<button data-sort-by=".cat-' . $id . '" class="btn iso-filter btn-sm">
							    				'. $image .'
							    				'. $name .'
							    			</button>';
							    $catID = $id;
							}

							// Array with the category names and essences
							$q[$button][] = $filter;
					}

					// Restore original Post Data
					wp_reset_postdata(); ?>

					<div class="page-section hide">
				        <div class="filters">
				            <div class="text-center">                    
				                <div class="btn-group iso-essences-page-filters" data-toggle="btn-group">
				                	<button class="btn iso-filter btn-sm active" data-sort-by="*">All</button>
									<?php 						
									    foreach ($q as $key => $values) {
									    	echo $key;
									    } 
								    ?>
								</div>
							</div>
						</div>
					</div>
				<?php
				endif;

				// Restore original Post Data
				wp_reset_postdata();
				?>
				
				<div class="page-section">					
					<div class="container u-no-padding">
						<ul class="iso-essences-page">
							<?php
							/* Start the Loop */
							while ( have_posts() ) : the_post();							

								/*
								 * Include the Post-Format-specific template for the content.
								 * If you want to override this in a child theme, then include a file
								 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
								 */

								get_template_part( 'template-parts/content', 'essence-item' );

							endwhile; ?>					
						</ul>
					</div>
				</div>

				<?php 
				pagination();

			else :

				get_template_part( 'template-parts/content', 'none' );

			endif; ?>				

		</main><!-- #main -->

<?php
get_sidebar();
get_footer();
