<?php
/**
 * The template for displaying units on homepage
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sodapop
 */
?>

<?php

$args = array(
	'post_type'              => array( 'accessory' ),
	'post_status'            => array( 'publish' ),
	'posts_per_page'         => '10',
);

// The Query
$query = new WP_Query( $args );
?>

<?php
// The Loop
if ( $query->have_posts() ) { ?>	

	<div class="container">
		<div class="accessories slider slider-mobile">
			<ul class="swiper-wrapper"> 

			<?php
			while ( $query->have_posts() ) { 
				$query->the_post();
					if ( has_post_thumbnail() ) {
                    	$description = get_field('description'); 
                    	$package_image = get_field('package_image'); ?>

                        <li class="accessory swiper-slide slide">
                            <div class="accessory__inner">                        
                                <figure class="accessory__image">
                                    <?php the_post_thumbnail(); ?>
                                </figure>
                                <div class="accessory__details">
                                    <div class="text-center">
                                        <h4 class="accessory__title">
                                        	<?php the_title(); ?>
                                        </h4>
			                            <?php
			                            if( !empty( $description ) ) : ?>                                                   
	                                        <div class="accessory__copy">
	                                            <p>
	                                            	<?php echo $description; ?>
	                                            </p>
	                                        </div>
										<?php
	                    				endif; ?>	                                        
                                    </div>
                                    <div class="text-center">
										<?php 
										if ( is_main_site() ) {
											echo '<a href="'. get_the_permalink() .'" class="btn btn-primary btn-more">Entdecken Sie mehr</a>';
										} else {
											echo '<a href="'. get_the_permalink() .'" class="btn btn-primary btn-more">Explore more</a>';									
										}
										?>
                                    </div>
                                </div>
                            </div>
                            <?php
                            if( !empty( $package_image ) ) : ?>
		                        <div class="accessory__footer">
		                        	<?php
		                        		echo 	'<figure class="accessory__package u-img-center">
		                        					<img src="'. $package_image['url'] .'" alt="'. $package_image['alt'] .'" />
		                        				</figure>';
		                        	?>
		                        </div>	                            
	                        <?php
	                    	endif; ?>
                        </li>

					<?php
					}
			}
			?>

			</ul>
		</div>
	</div>
<?php
} else {
	// No posts found
}

// Restore original Post Data
wp_reset_postdata();
      
    
