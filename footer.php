<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sodapop
 */

?>

	<footer id="colophon" class="site-footer">
		<?php 
			get_template_part( 'footer-widgets' );
			get_template_part( 'footer-credits' );
			get_template_part( 'footer-video' );
		?>
	</footer>

</div><!-- #page -->

<?php 
	get_template_part( 'navigation' );
	// get_template_part( 'expand-products' );
?>

<?php wp_footer(); ?>

</body>
</html>
