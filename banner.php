<?php
/**
 * The banner section
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sodapop
 */

    $banner_init     = get_field('banner_init');    
    $banner_bg_color = get_field('banner_bg_color');
    $banner_bg_image = get_field('banner_bg_image');
    $banner_color    = get_field('banner_color');    
    $banner_title    = get_field('banner_title');
    $banner_copy     = get_field('banner_copy');
?>

<?php
if( $banner_init ) : ?>

    <section class="banner-section"
        <?php
        if( !empty( $banner_bg_color ) || !empty( $banner_bg_image ) ) :
            echo 'style="
                    background-color: '. $banner_bg_color .'; 
                    background-image: url('. $banner_bg_image .');
                "';
        endif;
        ?> 
    >
        <header class="section-header">                
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6 col-md-offset-6">
                        <div class="banner-section-content">
                            <div class="banner-section-copy">                            
                                <?php
                                if( !empty( $banner_title ) ) : ?>
                                    <h2 class="u-font-artistic-secondary u-font-normal"
                                        <?php
                                        if( !empty( $banner_color ) ) :
                                            echo 'style="
                                                    color: '. $banner_color .'; 
                                                "';
                                        endif;
                                        ?> 
                                    >
                                        <?php echo $banner_title; ?>
                                    </h2>
                                <?php
                                endif;
                                if( !empty( $banner_copy ) ) : ?>
                                    <div class="copy"
                                        <?php
                                        if( !empty( $banner_color ) ) :
                                            echo 'style="
                                                    color: '. $banner_color .'; 
                                                "';
                                        endif;
                                        ?> 
                                    >
                                        <?php echo $banner_copy; ?>
                                    </div>
                                <?php
                                endif; ?>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>            
    </section>

<?php
endif; ?>