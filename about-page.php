<?php
/**
 * Template Name: About us
 *
 * The template for displaying about us page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sodapop
 */

get_header(); 

$gi_logo   	  	  = get_field('gi_logo');
$gi_title   	  = get_field('gi_title');
$gi_subtitle   	  = get_field('gi_subtitle');
$gi_description   = get_field('gi_description');

$success_copy_1   = get_field('success_copy_1');
$success_copy_2   = get_field('success_copy_2');
$success_copy_3   = get_field('success_copy_3');
$success_copy_4   = get_field('success_copy_4');
$success_copy_5   = get_field('success_copy_5');

$success_image_1  = get_field('success_image_1');
$success_image_2  = get_field('success_image_2');
$success_image_3  = get_field('success_image_3');
$success_image_4  = get_field('success_image_4');
$success_image_5  = get_field('success_image_5');

$email = get_field('email');
$tel_1 = get_field('tel_1');
$tel_2 = get_field('tel_2');
$tel_label_1 = get_field('tel_label_1');
$tel_label_2 = get_field('tel_label_2');
?>

	<main id="main" class="site-main">
		<?php
			// Page header			
			get_template_part( 'slider-about' );
		?>		
        <?php 
        if( !empty( $gi_description ) ) : ?>
        	<div class="page-section">      		
            	<div class="container">
            		<div class="row">
            			<div class="col-xs-12 col-md-8 col-md-offset-2">
		            		<?php
							if( !empty( $gi_title ) &&
								!empty( $gi_logo ) && 
								!empty( $gi_subtitle ) ) : ?>            									
			            		<header>
								    <figure>
								        <img src="<?php echo $gi_logo['url']; ?>" alt="<?php echo $gi_logo['alt']; ?>" class="about-logo" />
								    </figure>
					                <h2 class="h4">
					                	<?php echo $gi_title; ?>
					                </h2>
					                <div class="copy u-copy-lg">
					                	<?php echo $gi_subtitle ?>
					                </div>
			            		</header>
							<?php
							endif; ?>
		                	<div class="copy u-color-primary">
		                		<?php echo $gi_description; ?>
		                	</div>
            			</div>
            		</div>
            	</div>
        	</div>
        <?php
    	endif; ?>		

		<?php
		if( !empty( $success_copy_1 ) &&
			!empty( $success_image_1 ) &&
			!empty( $success_copy_2 ) &&
			!empty( $success_image_2 ) &&
			!empty( $success_copy_3 ) &&
			!empty( $success_image_3 ) &&			
			!empty( $success_copy_4 ) &&			
			!empty( $success_image_4 ) &&			
			!empty( $success_copy_5 ) &&
			!empty( $success_image_5 ) ) : ?>
		<section class="about-section">
            <header class="section-header">
                <div class="container">                      
					<?php 
					if ( is_main_site() ) {
						echo '<h2 class="h5 text-center">Grundlagen für den Erfolg</h2>';
					} else {
						echo '<h2 class="h5 text-center">Basics of success</h2>';
					}
					?>
                </div>
            </header>

            <div class="container">
                <div class="row">
                	<?php
                	if( !empty( $success_copy_1 ) &&
                		!empty( $success_image_1 ) ) : ?>
	                    <div class="col-xs-12 col-sm-6 col-md-2 col-md-offset-1">
	                        <figure class="u-img-center">
	                            <img src="<?php echo $success_image_1['url']; ?>" alt="<?php echo $success_image_1['alt']; ?>" />
	                        </figure>
	                        <br>
	                        <div class="text-center">
	                            <div class="copy u-text-xs">
	                                <?php echo $success_copy_1; ?>
	                            </div>
	                        </div>
	                    </div>
					<?php
					endif;

                	if( !empty( $success_copy_2 ) &&
                		!empty( $success_image_2 ) ) : ?>
	                    <div class="col-xs-12 col-sm-6 col-md-2">
	                        <figure class="u-img-center">
	                            <img src="<?php echo $success_image_2['url']; ?>" alt="<?php echo $success_image_2['alt']; ?>" />
	                        </figure>
	                        <br>
	                        <div class="text-center">
	                            <div class="copy u-text-xs">
	                                <?php echo $success_copy_2; ?>
	                            </div>
	                        </div>
	                    </div>
					<?php
					endif;

                	if( !empty( $success_copy_3 ) &&
                		!empty( $success_image_3 ) ) : ?>
	                    <div class="col-xs-12 col-sm-6 col-md-2">
	                        <figure class="u-img-center">
	                            <img src="<?php echo $success_image_3['url']; ?>" alt="<?php echo $success_image_3['alt']; ?>" />
	                        </figure>
	                        <br>
	                        <div class="text-center">
	                            <div class="copy u-text-xs">
	                                <?php echo $success_copy_3; ?>
	                            </div>
	                        </div>
	                    </div>
					<?php
					endif;

                	if( !empty( $success_copy_4 ) &&
                		!empty( $success_image_4 ) ) : ?>
	                    <div class="col-xs-12 col-sm-6 col-md-2">
	                        <figure class="u-img-center">
	                            <img src="<?php echo $success_image_4['url']; ?>" alt="<?php echo $success_image_4['alt']; ?>" />
	                        </figure>
	                        <br>
	                        <div class="text-center">
	                            <div class="copy u-text-xs">
	                                <?php echo $success_copy_4; ?>
	                            </div>
	                        </div>
	                    </div>
					<?php
					endif;

                	if( !empty( $success_copy_5 ) &&
                		!empty( $success_image_5 ) ) : ?>
	                    <div class="col-xs-12 col-sm-6 col-md-2">
	                        <figure class="u-img-center">
	                            <img src="<?php echo $success_image_5['url']; ?>" alt="<?php echo $success_image_5['alt']; ?>" />
	                        </figure>
	                        <br>
	                        <div class="text-center">
	                            <div class="copy u-text-xs">
	                                <?php echo $success_copy_5; ?>
	                            </div>
	                        </div>
	                    </div>
					<?php
					endif; ?>										
                </div>
            </div>
        </section>
        <?php
    	endif; ?>

		<?php
		if( !empty( $email ) &&
			!empty( $tel_1 ) &&
			!empty( $tel_label_1 ) &&
			!empty( $tel_2 ) &&
			!empty( $tel_label_2 ) ) : ?>
		<section class="about-section contact-us u-bg-primary">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-3">
                        <header class="section-header">
							<?php 
							if ( is_main_site() ) {
								echo '<h2 class="h5 u-color-default">Kontakt</h2>';
							} else {
								echo '<h2 class="h5 u-color-default">Contact</h2>';
							}
							?>                        	
                        </header>
						
						<?php
						if( !empty( $email ) ) : ?>
	                        <section class="contact-info">
	                            <h5 class="h6 text-uppercase u-color-default contact-info__headline">E-Mail</h5>
	                            <ul class="list">
	                                <li class="list__row">
	                                    <span class="list__col">
	                                        <a href="mailto:<?php echo $email; ?>" target="_blank" class="email-address"><?php echo $email; ?></a>
	                                    </span>
	                                </li>
	                            </ul>
	                        </section>
						<?php
						endif; ?>

                        <section class="contact-info">
							<?php 
							if ( is_main_site() ) {
								echo '<h5 class="h6 text-uppercase u-color-default contact-info__headline">Telefon</h5>';
							} else {
								echo '<h5 class="h6 text-uppercase u-color-default contact-info__headline">Phone</h5>';
							}
							?>
                            <ul class="list">
								<?php
								if( !empty( $tel_1 ) &&
									!empty( $tel_label_1 ) ) : ?>
	                                <li class="list__row">
	                                    <span class="list__col">
	                                        <a href="tel:<?php echo $tel_1; ?>" target="_blank"><?php echo $tel_label_1; ?></a>
	                                    </span>
	                                </li>
								<?php
								endif;
								if( !empty( $tel_2 ) &&
									!empty( $tel_label_2 ) ) : ?>
	                                <li class="list__row">
	                                    <span class="list__col">
	                                        <a href="tel:<?php echo $tel_2; ?>" target="_blank"><?php echo $tel_label_2; ?></a>
	                                    </span>
	                                </li>
								<?php
								endif; ?>	                                
                            </ul>
                        </section>
                    </div>

                    <div class="col-xs-12 col-md-4">
                    	<?php
                    		get_template_part( 'contact-form' ); 
                    	?>
                    </div>
                </div>
            </div>
        </section>
        <?php
    	endif; ?>        

	</main><!-- #main -->

<?php
get_footer();