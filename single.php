<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package sodapop
 */

get_header(); ?>

		<main id="main" class="site-main">
			
		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', get_post_type() );

			echo 	'<div class="section u-bg-gray-lightest">
						<div class="container-fluid u-no-padding">
							'. get_the_post_navigation() .'
						</div>
					</div>';

			// If comments are open or we have at least one comment, load up the comment template.
			if ( comments_open() || get_comments_number() ) :
				comments_template();
			endif;

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->

<?php
get_sidebar();
get_footer();
