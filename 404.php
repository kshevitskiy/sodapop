<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package sodapop
 */

get_header(); ?>

		<main id="main" class="site-main">

			<section class="page error-404 not-found">
				<header class="page-header">
					<div class="container">						
						<div class="text-center">						
							<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'sodapop' ); ?></h1>
						</div>
					</div>
				</header><!-- .page-header -->

				<div class="page-section">
					<div class="container">						
						<div class="row">
							<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
								<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try to search?', 'sodapop' ); ?></p>
								<br>
								<?php
									get_search_form();
								?>
							</div>
						</div>
					</div><!-- .container -->
				</div><!-- .page-section -->
			</section><!-- .error-404 -->

		</main><!-- #main -->

<?php
get_footer();
