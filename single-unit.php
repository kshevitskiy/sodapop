<?php
/**
 * The template for displaying all single units
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package sodapop
 */

get_header(); ?>

		<main id="main" class="site-main">

		<?php
		while ( have_posts() ) : the_post();

			get_template_part( 'template-parts/content', 'unit' );

			echo 	'<div class="section u-bg-gray-lightest">
						<div class="container-fluid u-no-padding">
							'. get_the_post_navigation() .'
						</div>
					</div>';

		endwhile; // End of the loop.
		?>

		</main><!-- #main -->

<?php
get_footer();