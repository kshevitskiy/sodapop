<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sodapop
 */

?>
<!doctype html>
<html <?php language_attributes(); ?> itemscope itemtype="http://schema.org/WebPage">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

	<?php 
		if( is_front_page() ) {
			echo '<meta name="theme-color" content="#003b56">';
		} else {
			echo '<meta name="theme-color" content="#262626">';
		}
	?>
	
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

    <header id="masthead" class="site-header">
        <div class="nav-bar">
        	<div class="nav-bar__inner">
	            <div class="site-header__brand">
	                <button type="button" class="btn btn-expand hide">
	                    <span class="btn-expand__dot"></span>
	                    <span class="sr-only">Expand</span>
	                </button>
					<?php
					the_custom_logo();
					if ( is_front_page() && is_home() ) : ?>
						<h1 class="site-header__title sr-only"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
					<?php else : ?>
						<p class="site-header__description sr-only"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
					<?php
					endif;

					$description = get_bloginfo( 'description', 'display' );
					if ( $description || is_customize_preview() ) : ?>
						<p class="site-header__description sr-only"><?php echo $description; /* WPCS: xss ok. */ ?></p>
					<?php
					endif; ?>
	            </div>
	            <div class="site-header__controls">
	                <div class="controls-group">
	                    <form action="&nbsp;" class="search-product hidden-xs hidden-sm hide">                       	                    	
	                    	<?php 
		                    	if ( is_main_site() ) {
		                    		echo	'<input type="search" class="quick-search" placeholder="Suchen nach ..." />
		                    				<button type="button" class="search-btn" title="Suchen"></button>';
		                    		
		                    	} else {
		                    		echo	'<input type="search" class="quick-search" placeholder="Search" />
		                    				<button type="button" class="search-btn" title="Search"></button>';
		                    	}
	                    	?>	                        	                        
	                    </form>
	                    <button type="button" class="cart-btn hidden-xs hidden-sm hide" title="My cart"></button>

						<?php get_template_part( 'header-lang' ); ?>
	                </div>                
	                <button type="button" class="menu-btn" title="Menu">
	                    <span class="sr-only">Menu</span>
	                    <span class="menu-btn__bar"></span>
	                    <span class="menu-btn__bar"></span>
	                    <span class="menu-btn__bar"></span>
	                    <span class="menu-btn__bar"></span>
	                </button>
	            </div>
	        </div>
        </div>

		<?php
		if ( !is_page_template('home-page.php') ) : ?>
	        <div class="sub-nav">
	            <figure class="sub-nav__image">
	                <img src="<?php echo get_template_directory_uri(); ?>/img/puyw.png" alt="#pop up your water">
	            </figure>
	        </div>
		<?php
		endif; ?>
    </header>	
