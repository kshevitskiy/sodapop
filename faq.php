<?php
/**
 * The template for displaying faq on homepage
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sodapop
 */
?>


<?php

$args = array(
	'post_type'      => array( 'question' ),
	'post_status'    => array( 'publish' ),    	
    'posts_per_page' => -1
);

// The Query
$query = new WP_Query( $args );
$q = array();

// The Loop
if ( $query->have_posts() ) { ?>	
	<section class="section faq-section u-bg-gray-lightest" id="faq">			
		<header class="section-header">
		    <div class="text-center">
				<?php 
				if ( is_main_site() ) {
					echo '<span class="pretitle text-uppercase">Häufig gestellte Fragen</span>                    
		        		  <h2 class="h4">Antworten auf die am häufigsten gestellten Fragen</h2>';
				} else {
					echo '<span class="pretitle text-uppercase">Frequently Asked Questions</span>                    
		        		  <h2 class="h4">Answers to the most common questions</h2>';
				}
				?>
		    </div>
		</header>		
		<div class="container">
			<div class="faq">

					<?php
					while ( $query->have_posts() ) { 
						$query->the_post();

						// FAQ categories
						$categories = get_the_category();
						foreach ( $categories as $key=>$category ) {
						    $b = '<a data-toggle="tab" href="#category' . $category->term_id . '" data-cat="category' . $category->term_id . '" class="btn btn-default btn-sm">' . $category->name . '</a>';    
						}

						// Question
						$id = get_the_ID();
						$title = get_the_title();
						$content = get_the_content();
						$question = '<div class="panel-heading" role="tab" id="question-heading-'. $id .'">
	                                    <h4 class="panel-title">
	                                        <a role="button" data-toggle="collapse" data-parent="#faq" href="#question-'. $id .'" aria-controls="question-'. $id .'">'. $title .'</a>
	                                    </h4>
	                                </div>
	                                <div id="question-'. $id .'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="question-heading-'. $id .'">
	                                    <div class="panel-body copy">'. $content .'</div>
	                                </div>';

						// Array with the category names and questions
						$q[$b][] = $question;
					}

					// Restore original Post Data
					wp_reset_postdata(); ?>

	                <div class="faq__header u-no-margin-top">
	                    <div class="text-center">                    
	                        <div class="btn-group" data-toggle="btn-group">
								<?php 						
							    foreach ($q as $key => $values) {
							    	echo $key;
							    } ?>
							</div>
						</div>
					</div>

	                <div class="faq__body">
	                    <div class="tab-content" role="tablist">
						    <?php
						    $i = -1;
						    foreach ($q as $key=>$values) {							    	
						        echo '<div id="category-'. $i++ .'" class="tab-pane fade">';
						        	echo '<div class="container">';
							        	echo '<div class="row">';
								            foreach ($values as $value) {
								                echo '<div class="panel panel-default col-xs-12 col-md-6">' . $value . '</div>';
								            }
								        echo '</div>';
								    echo '</div>';
						        echo '</div>';
						    }
							?>
						</div>
					</div>

			</div>
		</div>
	</section>
<?php
} else {
	// No posts found
}
?>