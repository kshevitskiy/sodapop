var DETECTOR = {
	
    detect : function() {
        return /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
    }
};

module.exports = {
    isMobile : DETECTOR.detect
};
