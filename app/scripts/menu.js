var MENU = {

	show: function(el, target) {
		$(el).on('click', function() {
			$(this).toggleClass('active');
			$(target).toggleClass('active');
		});
	},

	hide: function(el) {
		var $el = $(el);
		if ( $el.hasClass('active') ) {
			$el.click();
		}
	},

	menuItem: function(el) {
		var $el = $(el);
		$el.on('click', function() {
			MENU.hide('.menu-btn');
		});
	},

	init: function() {
		MENU.show('.menu-btn', '.site-menu');
		MENU.menuItem('.menu-item a');
	}
}


module.exports = {
    init: MENU.init
};