var SCROLL = {

    scrollTo: function(element, duration) {
        // Select all links with hashes
        $(element)
          // Remove links that don't actually link to anything
          .not('[href="#"]')
          .not('[href="#0"]')
          .click(function(event) {
            // On-page links
            if (
              location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
              && 
              location.hostname == this.hostname
            ) {              
              // Figure out element to scroll to
              var target = $(this.hash);              
              target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
              // Does a scroll target exist?
              if (target.length) {
                // Only prevent default if animation is actually gonna happen
                event.preventDefault();
                $('html, body').animate({
                  scrollTop: target.offset().top - 62
                }, duration);
              }
            }
          });
    },
  
    init: function() {
        SCROLL.scrollTo('a[href*="#"].to', 300);
        SCROLL.scrollTo('.menu-item a[href*="#"]', 300);
    }
}


module.exports = {
    init : SCROLL.init
};
