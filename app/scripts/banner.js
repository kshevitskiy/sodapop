var BANNER = {

	bg: function(el) {
		$(el).each(function() {
			var $this = $(this),
				bg = $this.find('.banner__bg'),
				src = bg.attr('src');				

			$this.css('backgroundImage', 'url(' + src + ')');
		});
	},

	init: function() {
		BANNER.bg('.banner');
	}
}


module.exports = {
    init: BANNER.init
};