var mobileDetector = require('./mobileDetector');
var viewport = $(window).width();

var UI = {
	
	btnGroup: function(element) {
		$(element).on('click', function(){
			var $this = $(this);
			$this.parent().find('.active').removeClass('active');
			$this.addClass('active');
		});	
	},

	tooltip: {

			init: function(element) {
				$(element).each(function() {
					var $this = $(this),
						height = $this.height(),
						tooltip = $this.find('.unit-link-tt'),
						allTooltips = $('.unit-link-tt');

					$this.on('click', function() {
						// allTooltips.fadeOut();

						if( tooltip.css('display') !== 'block' ) {
							$this.css('boxShadow', '0 0 0 15px rgba(101, 55, 241, .2)');
							tooltip.fadeIn();
						} else {
							$this.css('boxShadow', '0 0 0 0px rgba(101, 55, 241, .1)');
							tooltip.fadeOut();
						}
					});
				});
			},						
	},

	// expand: function(element) {
	// 	var products = $('#all-products');

	// 	$(element).on('click', function() {						
	// 		var $this = $(this);
	// 		products.modal('toggle');
	// 		$this.toggleClass('active');
	// 	});
	// },

	// searchBar: function(element) {
	// 	var products = $('#all-products');

	// 	$(element).on('click', function() {
	// 		var $this = $(this);					
	// 			parent = $this.parent(),
	// 			search = parent.find('.quick-search'),
	// 			expand = $('.btn-expand');

	// 		if( !parent.hasClass('active') && expand.hasClass('active') ) {
	// 			parent.toggleClass('active');				
	// 		} else {
	// 			parent.toggleClass('active');
	// 			products.modal('toggle');
	// 		}
			
	// 		if( !mobileDetector.isMobile() ) {
	// 			if ( parent.hasClass('active') ) {
	// 				setTimeout(function() {
	// 					search.focus();
	// 				}, 500);
	// 			}
	// 		}
	// 	});
	// },

	// productsFilter: function(filters, container, item) {
	// 	var qsRegex;
	// 	var filterValue;

	// 	var $container = $(container).isotope({
	// 		itemSelector: item,
	// 		layoutMode: 'fitRows',
	// 		filter: function() {
	// 			var $this = $(this);
	// 			var searchResult = qsRegex ? $this.text().match( qsRegex ) : true;
	// 			var buttonResult = filterValue ? $this.is( filterValue ) : true;
	// 			return searchResult && buttonResult;
	// 		}
	// 	});

	// 	$(filters).each(function() {
	// 		var $this = $(this);

	// 		$this.on( 'click', 'button', function() {
	// 			filterValue = $(this).attr('data-sort-by');
	// 			$container.isotope();
	// 		});

	// 		$this.on( 'change', function(e) {
	// 		    var optionSelected = $('option:selected', this);
	// 			filterValue = this.value;
	// 			$container.isotope();
	// 		});
	// 	});		

	// 	var $quicksearch = $('.quick-search').keyup( debounce( function() {
	// 		qsRegex = new RegExp( $quicksearch.val(), 'gi' );
	// 		$container.isotope();
	// 	}) );
		  
	// 	function debounce( fn, threshold ) {
	// 		var timeout;
	// 		return function debounced() {
	// 			if ( timeout ) {
	// 	      		clearTimeout( timeout );
	// 	  		}
	// 	    	function delayed() {
	// 				fn();
	// 				timeout = null;
	// 	  		}
	// 	    	setTimeout( delayed, threshold || 100 );
	// 		};
	// 	}
	// },

	// isotope: function(filters, container, item) {
	
	// 	$(container).each(function() {
	// 		var $this = $(this);

	// 		$this.isotope({
	// 			itemSelector: item,
	// 			layoutMode: 'fitRows',
	// 			stagger: 30,
	// 		});
	// 	});

	// 	$(filters).each(function() {
	// 		var $this = $(this);

	// 		$this.on( 'click', 'button', function() {
	// 			var filterValue = $(this).attr('data-sort-by');
	// 			$(container).isotope({ filter: filterValue });
	// 		});

	// 		$this.on( 'change', function(e) {
	// 		    var optionSelected = $('option:selected', this),
	// 				filterValue = this.value;
	// 			$(container).isotope({ filter: filterValue });
	// 		});
	// 	});
	// },

	// allProducts: {

	// 	image: function(element) {
	// 		$(element).each(function() {
	// 			var $this   = $(this),
	// 				image   = $this.find('img'),
	// 				imgSrc  = image.attr('src');

	// 				$this.css('backgroundImage', 'url(' + imgSrc + ')');
	// 		});
	// 	}
	// },

	init: function() {
		UI.btnGroup('[data-toggle="btn-group"] .btn');
		UI.tooltip.init('.unit-link');
		// UI.expand('.btn-expand');
		// UI.searchBar('.search-btn');

		// if ( $('.iso-filters').length ) {			
		// 	$('#all-products').on('shown.bs.modal', function () {
		// 		UI.productsFilter('.iso-filters', '.iso-items', '.iso-item');
		// 	});
		// }

		// if ( $('.iso-essence-filters').length ) {
		// 	if (mobileDetector.isMobile() && viewport >= 768) {
		// 		UI.isotope('.iso-essence-filters', '.iso-essences', '.iso-item');
		// 	} else if(!mobileDetector.isMobile()) {
		// 		UI.isotope('.iso-essence-filters', '.iso-essences', '.iso-item');
		// 	}
		// }

		// if ( $('.iso-essences-page-filters').length ) {
		// 	UI.isotope('.iso-essences-page-filters', '.iso-essences-page', '.iso-item');
		// }		

		// UI.allProducts.image('.product-item__image');
	}
}

module.exports = {
    init : UI.init
};