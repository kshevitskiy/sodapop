var SECTION = {

    background: function(element) {        
        $(element).each(function() {
            var el = $(this),
                img = el.find('img'),
                src = img.attr('src');

            el.css('backgroundImage', 'url( ' + src + ' )');
        });
    },
  
    init: function() {
        SECTION.background('.section-background');
    }
}


module.exports = {
    init : SECTION.init
};