var OPTIMIZE = {	
	
	visibility: function(element) {
		var elements = document.querySelectorAll(element);
		if(elements) {
			elements.forEach( function(element, index) {
				element.removeAttribute('style');
			});
		}		
	},

	init: function() {
		window.onload = function() {
			OPTIMIZE.visibility('.banners-slider');
			OPTIMIZE.visibility('.about-slider');
		}
	}
}

module.exports = {
    init : OPTIMIZE.init
};