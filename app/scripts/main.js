var swiper = require('./swiper'),
    mobileDetector = require('./mobileDetector'),
    menu = require('./menu'),
    animation = require('./animation'),
    sections = require('./sections')    
    scroll = require('./scroll'),
    // banner = require('./banner'),
    ui = require('./ui'),
    faq = require('./faq'),
    video = require('./video'),
    cssFilter = require('./css-filter'),
    optimize = require('./optimize');

menu.init();
sections.init();
// banner.init();
ui.init();
faq.init();
video.init();
if (typeof Swiper !== 'undefined') {
    swiper.init();
}
scroll.init();
cssFilter.init();
// animation.inview();
optimize.init();