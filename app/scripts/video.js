var modal = $('.modal-video'),
	wrapper = $('.modal-video').find('.video-wrapper');

var VIDEO = {	
	
	src: function(element) {
		$(element).each(function(){
			var $this = $(this),
				play = $(this).find('.btn-play'),
				src = $this.find('iframe');

			VIDEO.playVideo(play, src);
		});
	},

	playVideo: function(button, source) {
		button.on('click', function() {

			modal.on('shown.bs.modal', function () {
				wrapper.append(source);					
			});

			modal.on('hidden.bs.modal', function () {
				source.remove();
			});				
		});
	},

	init: function() {
		VIDEO.src('.video');
	}
}

module.exports = {
    init : VIDEO.init
};