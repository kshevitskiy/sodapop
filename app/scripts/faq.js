var FAQ = {

    tabID: function(element) {        
        var header = $(element).find('.faq__header'),
            cat    = header.find('.btn'),
            tab    = $(element).find('.tab-pane');

        var catID = cat.map(function() {
            return $(this).data('cat');
        }).get();

        tab.each(function(index) {
            var $this  = $(this),
                tabID  = $this.attr('id', catID[index]);            
        });

        $('[data-cat="' + catID[0] + '"]').addClass('active');
        $('#' + catID[0] + '').addClass('in active');
    },
  
    init: function() {
        FAQ.tabID('.faq');
    }
}


module.exports = {
    init : FAQ.init
};