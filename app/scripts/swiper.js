var mobileDetector = require('./mobileDetector');
var viewport = $(window).width();

var SWIPER = {

	banners: function(slider) {
		var swiper = new Swiper(slider, {    
		    slidesPerView: 1,
			pagination: {
				el: '.pagination',
				clickable: true,
			},
			autoplay: {
				delay: 3000,
			},
		    speed: 1000,
		    // loop: true,
		    // preloadImages: false,
		    // lazy: true,
		});
	},	

	essences: function(slider) {
		var swiper = new Swiper(slider, {    
		    slidesPerView: 'auto',
		});
	},

	accessories: function(slider) {
		var swiper = new Swiper(slider, {    
		    slidesPerView: 'auto',		    
			breakpoints: {
			    480: {
					spaceBetween: 20,
			    },
			},
		});
	},

	partners: function(slider) {
		var swiper = new Swiper(slider, {    
		    slidesPerView: 'auto',
		    spaceBetween: 20,
		    autoplay: true,
		    loop: true,
		    speed: 2000,
		    freeMode: true,	    
		});
	},	

	about: function(slider) {
		var swiper = new Swiper(slider, {    
		    slidesPerView: 3,
		    spaceBetween: 10,
		    autoplay: true,
		    loop: true,
		    speed: 1000,
			autoplay: {
				delay: 500,
			},		    
			breakpoints: {
				768: {
				  slidesPerView: 2
				},
				480: {
				  slidesPerView: 'auto',
				  centeredSlides: true
				}
			}		    
		});
	},

	init: function() {
		SWIPER.banners('.banners-slider');
		SWIPER.about('.about-slider');

		// Mobile sliders
		if (mobileDetector.isMobile() && viewport < 768) {
			SWIPER.essences('.essences');
			SWIPER.accessories('.accessories');
		}
		if (mobileDetector.isMobile()) {
			SWIPER.partners('.partners');			
		}		
	}
}

module.exports = {
    init : SWIPER.init
};
