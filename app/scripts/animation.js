var ANIMATION = {

    inview: function() {
        inView.threshold(.25);

        inView('[data-animation="slideUp"]').on('enter', function(e) {
            setTimeout(function() {
                e.style.transform = 'translateY(0)';
            }, 250);
        });

        inView('[data-animation="fadeIn"]').on('enter', function(e) {
            setTimeout(function() {
                e.style.opacity = 1;
            }, 250);
        });        

        inView('[data-animation="fadeInUp"]').on('enter', function(e) {
            setTimeout(function() {
                e.setAttribute('style', 'transform: translateY(0); opacity: 1;');
            }, 250);
        });

        inView('[data-animation="scaleIn"]').on('enter', function(e) {
            setTimeout(function() {
                e.style.transform = 'scale(1)';
            }, 250);
        });        
    },
};

module.exports = {
    inview: ANIMATION.inview
};