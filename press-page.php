<?php
/**
 * Template Name: Pressroom
 *
 * The template for displaying pressroom page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sodapop
 */

get_header(); 

$contact_one = get_field('contact_one');
$contact_two = get_field('contact_two');
$file = get_field('file');
?>

	<main id="main" class="site-main">
		<?php
			// Page header
			get_template_part( 'page-header' );
		?>

		<div class="press">
	    	<?php
	    		get_template_part( 'press' );
	    	?>

			<section class="press-section press-contact">                        
                <header class="section-header">
                    <div class="conteiner">
                        <div class="text-center">                                    
                            <h2 class="h4 u-color-default">mySodapop Pressekontakt</h2>
                        </div>
                    </div>
                </header>

                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-4 col-md-offset-2">
                            <div class="copy">
								<?php
									if( !empty( $contact_one ) ) :
										echo $contact_one;
									endif;
								?>

                            </div>
                        </div>

                        <div class="col-xs-12 col-sm-6 col-md-4">
							<?php
								if( !empty( $contact_two ) ) :
									echo $contact_two;
								endif;
							?>                        	
                        </div>
                    </div>
                </div>


				<?php
					if( !empty( $file ) ) : ?>
					
	                <footer class="press-contact-footer">
	                    <div class="text-center">                                
	                        <a href="<?php echo $file; ?>" class="btn btn-default btn-download" target="_blank">Download Messefolder</a>
	                    </div>
	                </footer>

				<?php	
				endif; ?>
            </section>	    	
	    </div>

	</main><!-- #main -->

<?php
get_footer();