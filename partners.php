<?php
/**
 * The template for displaying units on homepage
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sodapop
 */
?>

<?php

$args = array(
	'post_type'              => array( 'partner' ),
	'post_status'            => array( 'publish' ),
	'posts_per_page'         => '10',
);

// The Query
$query = new WP_Query( $args );
?>

<?php
// The Loop
if ( $query->have_posts() ) { ?>	

	<div class="section partners-section">
		<div class="container u-no-padding">
			<div class="partners slider">
				<ul class="swiper-wrapper">

				<?php
				while ( $query->have_posts() ) { 
					$query->the_post(); 
						if ( has_post_thumbnail() ) : ?>
	                        <li class="partner swiper-slide slide">	                            
	                            <?php the_post_thumbnail( 'full', array('class' => 'partner__logo') ); ?>
	                        </li>		                    				        
						<?php
						endif;
				}
				?>
				</ul>
			</div>
		</div>
	</div>

<?php
} else {
	// No posts found
}

// Restore original Post Data
wp_reset_postdata();
      
    
