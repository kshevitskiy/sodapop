<?php
/**
 * The template for displaying accesories section on homepage
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sodapop
 */

$accessories_pre_title = get_field('accessories_pre_title');
$accessories_headline = get_field('accessories_headline');
$accessories_background = get_field('accessories_background');
// $accessories_link = get_field('accessories_link');

if( !empty( $accessories_pre_title ) && 
    //!empty( $accessories_link ) &&
	!empty( $accessories_headline ) ) : ?>	
    <section class="section accessories-section" id="bottles">    
        <div class="section-container u-bg-gray-lightest">
            <header class="section-header">
                <div class="container">
                    <div class="text-center">                                                
                        <span class="pretitle text-uppercase">
                        	<?php echo $accessories_pre_title; ?>
                        </span>
                        <h2 class="h4">
                        	<?php echo $accessories_headline; ?>
                        </h2>
                        <?php
                            // echo '<a href="'. $accessories_link .'" class="btn btn-link">View all accesories</a>';
                        ?>
                    </div>
                </div>
            </header>

            <?php get_template_part( 'accessories' ); ?>

        </div>
    </section>				
<?php
endif;

if( !empty( $accessories_background ) ) : ?>
    <div class="section">
        <div class="section-container section-background u-bg-gray-lightest">
            <img src="<?php echo $accessories_background['url']; ?>" alt="<?php echo $accessories_background['alt']; ?>" class="u-fade-out" />
        </div>                        
    </div>
<?php
endif;    