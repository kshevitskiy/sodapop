<?php
/**
 * The template for displaying carbon dioxide section on homepage
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sodapop
 */

$co2_pre_title = get_field('co2_pre_title');
$co2_headline = get_field('co2_headline');
$co2_copy_large = get_field('co2_copy_large');
$co2_image = get_field('co2_image');
$co2_badge = get_field('co2_badge');
$co2_copy = get_field('co2_copy');
$co2_link = get_field('co2_link');
if( !empty( $co2_pre_title ) && 
	!empty( $co2_headline )  &&
	!empty( $co2_copy_large ) && 
    !empty( $co2_image ) &&
    !empty( $co2_copy ) && 
	!empty( $co2_link ) ) : ?>

    <section class="section co2-section" id="co2-refil">
        <header class="section-header">
            <div class="container"> 
                <div class="text-center"> 
                    <span class="pretitle text-uppercase u-color-default">
                        <?php echo $co2_pre_title; ?>
                    </span>                           
                    <h2 class="h4 u-color-default">
                        <?php echo $co2_headline; ?>
                    </h2>
                    <div class="copy u-copy-lg copy--color-default">                            
                        <?php echo $co2_copy_large; ?>
                    </div>
                </div>
            </div>
        </header>
        <div class="container">
            <div class="product product-two-col">
                <div class="row sm flex">
                    <figure class="product__image col-xs-12 col-sm-4 col-md-4 col-md-offset-1">
                        <div class="text-center">
                            <figure class="u-img-center">                                
                                <img src="<?php echo $co2_image['url']; ?>" alt="<?php echo $co2_image['alt']; ?>" />
                                <?php 
                                if ( !empty( $co2_image ) ) {
                                    echo    '<div class="product__badge">
                                                <img src="'. $co2_badge['url'] .'" alt="'. $co2_badge['alt'] .'">
                                            </div>';
                                }
                                ?>
                                <span class="one-for-all-badge">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/one4all.png" alt="One for all">
                                </span>                                
                            </figure>                            
                        </div>
                    </figure>
                    <div class="col-xs-12 col-sm-8 col-md-6">
                        <div class="product__summary copy copy--color-default">
                            <?php echo $co2_copy; ?>
                        </div>                        
                        <?php 
                        if ( is_main_site() ) {
                            echo '<a href="'. $co2_link .'" class="product__btn btn btn-primary btn-more">Entdecken Sie mehr</a>';
                        } else {
                            echo '<a href="'. $co2_link .'" class="product__btn btn btn-primary btn-more">Explore more</a>';
                        }
                        ?>                        
                    </div>
                </div>
            </div>
        </div>
    </section>    
<?php
endif;