<?php
/**
 * Contact form
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package sodapop
 */

echo do_shortcode(' [contact-form-7 id="386" title="Contact us"] '); 

?>