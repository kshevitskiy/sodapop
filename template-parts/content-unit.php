<?php
/**
 * Template part for displaying unit
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sodapop
 */

?>

<?php
	$subtitle = get_field('subtitle');
	$summary  = get_field('summary');
	$details  = get_field('details');

	$specs_model 	   = get_field('model_number');
	$specs_measurments = get_field('measurments');
	$specs_weight 	   = get_field('weight');
	$specs_material    = get_field('material');

	$pkg_item_1 = get_field('package_item_1');
	$pkg_icon_1 = get_field('package_icon_1');
	$pkg_item_2 = get_field('package_item_2');
	$pkg_icon_2 = get_field('package_icon_2');	
	$pkg_item_3 = get_field('package_item_3');
	$pkg_icon_3 = get_field('package_icon_3');	

	$video = get_field('video');

	$bg_color = get_field('background_color');

	$product_certs = get_field('product_certs');
?>

<div id="product-<?php the_ID(); ?>" class="product">

	<section class="product-section product-intro-section"
	    <?php
	    if( !empty( $bg_color ) ) :
	        echo 'style="
	        		background-color: '. $bg_color .'; 
	        	"';
	    endif;
	    ?> 
	>   			
		<div class="container">
			<?php
			if( wp_is_mobile() ) : ?>
			<div class="row">
				<div class="col-xs-12 col-md-12">
					<header class="product-headline">
						<span class="pretitle text-uppercase">							
							<?php 
							if ( is_main_site() ) {
								echo '<span>Treffen</span>';
							} else {
								echo '<span>Meet</span>';									
							}
							?>								
							<span><?php the_title(); ?></span>											
						</span>
						<?php
		                if( !empty( $subtitle ) ) : ?>
		                	<h1 class="h4">
		                		<?php echo $subtitle; ?>
		                	</h1>
						<?php
						endif; ?>
					</header>					
				</div>
			</div>
			<?php
			endif; ?>
			<div class="row u-reverse-mobile">
				<div class="col-xs-12 col-sm-6 col-md-5">
					<?php
					if( !wp_is_mobile() ) : ?>					
					<header class="product-headline">
						<span class="pretitle text-uppercase u-color-gray">
							<?php 
							if ( is_main_site() ) {
								echo '<span>Treffen</span>';
							} else {
								echo '<span>Meet</span>';									
							}
							?>
							<span><?php the_title(); ?></span>											
						</span>
						<?php
                        if( !empty( $subtitle ) ) : ?>
                        	<h1 class="h4">
                        		<?php echo $subtitle; ?>
                        	</h1>
						<?php
        				endif; ?>
					</header>
					<?php 
					endif; ?>

                    <?php					
                    if( !empty( $summary ) ) : ?>
                    	<div class="product-summary">
                    		<div class="copy u-color-gray">
                    			<?php echo $summary; ?>
                    		</div>
                    	</div>
					<?php
    				endif;

    				if( !empty( $details ) ) : ?>
						<figure class="product-details">		    						
							<img src="<?php echo $details['url']; ?>" alt="<?php echo $details['alt']; ?>" />
						</figure>
					<?php
    				endif; ?>					
				</div>
				<?php
				if ( has_post_thumbnail() ) : ?>
					<div class="col-xs-12 col-sm-6 col-md-7">	    					
						<figure class="product-image u-img-center">
							<?php the_post_thumbnail('unit-image');

							if( !empty( $video ) ) : ?>
								<div class="video">								
									<button type="button" class="btn btn-play" data-toggle="modal" data-target="#video">
										<span class="btn-play__icon"></span>
										<?php 
										if ( is_main_site() ) {
											echo '<span class="btn-play__text">Video abspielen</span>';
										} else {
											echo '<span class="btn-play__text">Play Video</span>';									
										}
										?>										
									</button>
									<div class="video-src hide">
										<?php echo $video; ?>
									</div>
								</div>
							<?php
							endif; ?>

							<?php
							if( !empty( $product_certs ) ) : ?>
								<div class="copy">									
									<?php echo $product_certs; ?>
								</div>
							<?php
							endif; ?>							
						</figure>
					</div>
				<?php
				endif; ?>
			</div>
		</div>
	</section>

	<section class="product-section">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-5">
					<div class="row">						
	            	<?php
	            		if( !empty( $specs_model ) || 
	            			!empty( $specs_measurments ) ||
	            			!empty( $specs_weight ) ||
	            			!empty( $specs_material ) ) : ?>
							<section class="product-specs col-xs-12 col-sm-6 col-md-12">    								
								<?php 
								if ( is_main_site() ) {
									echo '<h4 class="h6">Technische Details</h4>';
								} else {
									echo '<h4 class="h6">Specifications</h4>';									
								}
								?>
		                        <ul class="list spec-list">
		                        	<?php
		                        		if( !empty( $specs_model ) ) : ?>
			                            <li class="list__row">			                                
											<?php 
											if ( is_main_site() ) {
												echo '<span class="list__col">Modellnummer:</span>';
											} else {
												echo '<span class="list__col">Model number:</span>';									
											}
											?>				                                
			                                <span class="list__col">
			                                	<?php echo $specs_model; ?>
			                                </span>
			                            </li>
			                        <?php 
			                        endif;
			                        	if( !empty( $specs_measurments ) ) : ?>
			                            <li class="list__row">
											<?php 
											if ( is_main_site() ) {
												echo '<span class="list__col">Abmessungen:</span>';
											} else {
												echo '<span class="list__col">Measurments:</span>';									
											}
											?>			                                
			                                <span class="list__col">
			                                	<?php echo $specs_measurments; ?>
			                                </span>
			                            </li>
			                        <?php
			                    	endif;
			                    		if( !empty( $specs_weight ) ) : ?>
			                            <li class="list__row">
											<?php 
											if ( is_main_site() ) {
												echo '<span class="list__col">Gewicht:</span>';
											} else {
												echo '<span class="list__col">Weight:</span>';									
											}
											?>				                                
			                                <span class="list__col">
			                                	<?php echo $specs_weight; ?>
			                                </span>
			                            </li>
			                        <?php
			                    	endif;
			                    		if( !empty( $specs_material ) ) : ?>          
			                            <li class="list__row">			                                
											<?php 
											if ( is_main_site() ) {
												echo '<span class="list__col">Material:</span>';
											} else {
												echo '<span class="list__col">Material:</span>';									
											}
											?>			                                
			                                <span class="list__col">
			                                	<?php echo $specs_material; ?>
			                                </span>
			                            </li>
			                        <?php
			                    	endif; ?>
		                        </ul>                                
							</section>
						<?php
						endif;

	            		if( !empty( $pkg_item_1 ) && !empty( $pkg_icon_1 ) || 
	            			!empty( $pkg_item_2 ) && !empty( $pkg_icon_2 ) ||
	            			!empty( $pkg_item_3 ) && !empty( $pkg_icon_3 ) ) : ?>
							<section class="product-specs col-xs-12 col-sm-6 col-md-12">								
								<?php 
								if ( is_main_site() ) {
									echo '<h4 class="h6">Lieferumfang</h4>';
								} else {
									echo '<h4 class="h6">Package contains</h4>';									
								}
								?>								
		                        <ul class="list spec-list u-flexbox u-no-wrap">
		                        	<?php
		                        		if( !empty( $pkg_item_1 ) && !empty( $pkg_icon_1 ) ) : ?>
			                            <li class="list__row">
			                                <div class="list__col list__block">
			                                    <figure class="u-img-center">
			                                    	<div class="list-icon">
			                                        	<img src="<?php echo $pkg_icon_1['url']; ?>" alt="<?php echo $pkg_icon_1['alt']; ?>" />
			                                        </div>
			                                        <div class="u-text-xs">
			                                        	<?php echo $pkg_item_1; ?>
			                                        </div>
			                                    </figure>
			                                </div>
			                            </li>
			                        <?php
			                    	endif;
			                    		if( !empty( $pkg_item_2 ) && !empty( $pkg_icon_2 ) ) : ?>
			                            <li class="list__row">
			                                <div class="list__col list__block">
			                                    <figure class="u-img-center">
			                                    	<div class="list-icon">
			                                        	<img src="<?php echo $pkg_icon_2['url']; ?>" alt="<?php echo $pkg_icon_2['alt']; ?>" class="bottle-item" />
			                                        </div>
			                                        <div class="u-text-xs">
			                                        	<?php echo $pkg_item_2; ?>
			                                        </div>
			                                    </figure>
			                                </div>
			                            </li>
									<?php
									endif;
										if( !empty( $pkg_item_3 ) && !empty( $pkg_icon_3 ) ) : ?>
			                            <li class="list__row">
			                                <div class="list__col list__block">
			                                    <figure class="u-img-center">
			                                    	<div class="list-icon">
			                                        	<img src="<?php echo $pkg_icon_3['url']; ?>" alt="<?php echo $pkg_icon_3['alt']; ?>" class="co2-cylinder-item" />
			                                    	</div>
			                                        <div class="u-text-xs">
			                                        	<?php echo $pkg_item_3; ?>
			                                        </div>
			                                    </figure>
			                                </div>
			                            </li>
									<?php
									endif; ?>		                            
		                        </ul>                                
							</section>
						<?php
						endif; ?>
					</div>
				</div>
				<div class="col-xs-12 col-md-7">
					<div class="product-description">						
						<?php 
						if ( is_main_site() ) {
							echo '<h4 class="h6">Produktbeschreibung</h4>';
						} else {
							echo '<h4 class="h6">About the product</h4>';									
						}
						?>							
						<div class="copy">    								
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>	

	<footer class="product-section hide">
		<div class="container">			
			<?php sodapop_entry_footer(); ?>
		</div>
	</footer><!-- .entry-footer -->
</div><!-- #product-<?php the_ID(); ?> -->
