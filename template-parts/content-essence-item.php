<?php
/**
 * Template part for displaying essence
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sodapop
 */

?>	

<?php
	// Categories
	$categories = get_the_category();	
	foreach ( $categories as $key=>$category ) {
		$id = $category->term_id;
	    $catID = $id;
	}

	// Essence
	$title 		 = get_the_title();
	$link 		 = get_the_permalink();
	$lang		 = '';
	if ( is_main_site() ) {
		$lang = '<a href="'. $link .'" class="essence__details btn btn-more">Entdecken Sie mehr</a>';
	} else {
		$lang = '<a href="'. $link .'" class="essence__details btn btn-more">Explore more</a>';
	}

	$badge 		 = get_field('badge'); 	
	$id  		 = get_field('product_id');
	$has_id  = '';
    if( !empty( $id ) ) {
    	$has_id  = '<div class="product__id u-color-default u-text-xs copy">'. $id .'</div>';
    }	
	$thumb_size  = '';											
	$thumb 		 = get_the_post_thumbnail($thumb_size);
	if ( wp_is_mobile() ) {
		$thumb_size = 'essence-thumb-mobile';
	} else {
		$thumb_size = 'essence-thumb';
	}
	$has_badge	 = '';
	if ( !empty( $badge ) ) {
		$has_badge = '<span class="essence__badge badge"><img src="'. $badge['url'] .'" alt="'. $badge['alt'] .'" /></span>';
	}
	$description = get_field('description');							
	$has_desc 	 = '';
	if ( !empty( $description ) ) {
		$has_desc = '<div class="essence__summary"><p>'. $description .'</p></div>';
	}							
	$has_cat	 = '';
	if( has_category(array($category->name)) ) {
		$has_cat = 'cat-'. $catID .'';
	}
	$bg_color 	 = get_field('background_color');

	$essence 	 = '<li class="essence iso-item col-xs-12 col-sm-6 col-md-4 '. $has_cat .'" style="background-color: '. $bg_color .'">
                    <div class="row sm flex">
                        <figure class="essence__image u-img-center col-xs-5 col-sm-4">
                        	'. $thumb .'
                            '. $has_badge .'
                        </figure>	                                
                        <div class="essence__content col-xs-7 col-sm-8">
                        	<br class="visible-xs">
                        	'. $has_id .'
                            <h4 class="essence__title">'. $title .'</h4>
                            '. $has_desc .'
                            '. $lang .'
                        </div>
                    </div>
                </li>';

    echo $essence;
?>
