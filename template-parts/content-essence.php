<?php
/**
 * Template part for displaying essence
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sodapop
 */

?>

<?php
	$subtitle = get_field('subtitle');
	$summary  = get_field('summary');

	$id = get_field('product_id');

	$calories = get_field('calories');
	$calories_lang = '';
	if ( is_main_site() ) {
		$calories_lang = 'Brennwert:';
	} else {
		$calories_lang = 'Energy:';
	}

	$fat = get_field('fat');
	$fat_lang = '';
	if ( is_main_site() ) {
		$fat_lang = 'Fett:';
	} else {
		$fat_lang = 'Fat:';
	}

	$saturated = get_field('saturated');
	$saturated_lang = '';
	if ( is_main_site() ) {
		$saturated_lang = 'Saturated:';
	} else {
		$saturated_lang = 'Saturated:';
	}

	$carbohydrates = get_field('carbohydrates');
	$carbohydrates_lang = '';
	if ( is_main_site() ) {
		$carbohydrates_lang = 'Kohlenhydate:';
	} else {
		$carbohydrates_lang = 'Carbohydrate:';
	}

	$sugar = get_field('sugar');
	$sugar_lang = '';
	if ( is_main_site() ) {
		$sugar_lang = 'Davon Zucker:';
	} else {
		$sugar_lang = 'Sugar:';
	}	

	$roughage = get_field('roughage');
	$roughage_lang = '';
	if ( is_main_site() ) {
		$roughage_lang = 'Ballaststoffe:';
	} else {
		$roughage_lang = 'Ballaststoffe:';
	}	

	$protein = get_field('protein');
	$protein_lang = '';
	if ( is_main_site() ) {
		$protein_lang = 'Eiweiβ:';
	} else {
		$protein_lang = 'Protein:';
	}	

	$salt = get_field('salt');
	$salt_lang = '';
	if ( is_main_site() ) {
		$salt_lang = 'Salz:';
	} else {
		$salt_lang = 'Salt:';
	}

	$niacin = get_field('niacin');
	$niacin_lang = '';
	if ( is_main_site() ) {
		$niacin_lang = 'Niacin:';
	} else {
		$niacin_lang = 'Niacin:';
	}

	$pantothenic_acid = get_field('pantothenic_acid');
	$pantothenic_acid_lang = '';
	if ( is_main_site() ) {
		$pantothenic_acid_lang = 'Pantothensäure:';
	} else {
		$pantothenic_acid_lang = 'Pantothenic acid:';
	}

	$vitamin_b6 = get_field('vitamin_b6');
	$vitamin_b6_lang = '';
	if ( is_main_site() ) {
		$vitamin_b6_lang = 'Vitamin B6:';
	} else {
		$vitamin_b6_lang = 'Vitamin B6:';
	}

	$vitamin_b12 = get_field('vitamin_b12');
	$vitamin_b12_lang = '';
	if ( is_main_site() ) {
		$vitamin_b12_lang = 'Vitamin B12:';
	} else {
		$vitamin_b12_lang = 'Vitamin B12:';
	}

	$video = get_field('video');

	$bg_color = get_field('background_color');
	$bg_image = get_field('background_image');
?>

<div id="product-<?php the_ID(); ?>" class="product">

	<section class="product-section product-intro-section"
	    <?php
	    if( !empty( $bg_color ) || !empty( $bg_image ) ) :
	        echo 'style="
	        		background-color: '. $bg_color .'; 
	        		background-image: url('. $bg_image .');
	        	"';
	    endif;
	    ?> 
	>   			
		<div class="container">
			<div class="row u-flexbox u-flex-align-center u-reverse-mobile">
				<div class="col-xs-12 col-sm-6 col-md-6">	    					
					<header class="product-headline">
                        <a href="javascript:history.back()" class="back-link text-uppercase">Back</a>
						<?php
                        if( !empty( $id ) ) :
                        	echo '<div class="product__id u-color-default">'. $id .'</div>';
						endif;                        	
                        ?>
    					<h1 class="h1 u-color-default u-no-margin-top">
    						<?php the_title(); ?>
    					</h1>
                        <?php
                        if( !empty( $subtitle ) ) : ?>
                        	<h3 class="h4 subtitle u-color-default">
                        		<?php echo $subtitle; ?>
                        	</h3>
						<?php
        				endif; ?>    					                        
					</header>					
                    <?php
                    if( !empty( $summary ) ) : ?>
                    	<div class="product-summary">
                    		<div class="copy u-color-default">
                    			<?php echo $summary; ?>
                    		</div>
                    	</div>
					<?php
    				endif; ?>					
				</div>
				<?php
				if ( has_post_thumbnail() ) : ?>
					<div class="col-xs-12 col-sm-6 col-md-6">	    					
						<figure class="product-image u-img-center">
							<?php the_post_thumbnail('essence-image');

							if( !empty( $video ) ) : ?>
								<div class="video">								
									<button type="button" class="btn btn-play" data-toggle="modal" data-target="#video">
										<span class="btn-play__icon"
											<?php
											if( !empty( $bg_color ) ) :
											    echo 'style="
											    		background-color: '. $bg_color .';
											    	"';
											endif;
											?> 
										></span>
										<?php 
										if ( is_main_site() ) {
											echo '<span class="btn-play__text">Video abspielen</span>';
										} else {
											echo '<span class="btn-play__text">Play Video</span>';									
										}
										?>										
									</button>
									<div class="video-src hide">
										<?php echo $video; ?>
									</div>
								</div>
							<?php
							endif; ?>
						</figure>
					</div>
				<?php
				endif; ?>
			</div>
		</div>
	</section>

	<section class="product-section">
		<div class="container">
			<div class="row">
				<div class="col-xs-12 col-md-5">
					<section class="product-specs">
						<?php
						if ( is_main_site() ) {
							echo '<h4 class="h6">Nährwertangaben</h4>';
						} else {
							echo '<h4 class="h6">Nutrition facts</h4>';
						}
						?>
                        <ul class="list spec-list">
                        	<?php
                    		if( !empty( $calories ) ) :
                    			echo	'<li class="list__row">		                                
                                			<span class="list__col">'. $calories_lang .'</span>
                                			<span class="list__col">'. $calories .'</span>
                            			</li>';
                    		endif;

                    		if( !empty( $fat ) ) :
                    			echo	'<li class="list__row">		                                
                                			<span class="list__col">'. $fat_lang .'</span>
                                			<span class="list__col">'. $fat .'</span>
                            			</li>';
                    		endif;

                    		if( !empty( $saturated ) ) :
                    			echo	'<li class="list__row">		                                
                                			<span class="list__col">'. $saturated_lang .'</span>
                                			<span class="list__col">'. $saturated .'</span>
                            			</li>';
                    		endif; 
                    		
                    		if( !empty( $carbohydrates ) ) :
                    			echo	'<li class="list__row">		                                
                                			<span class="list__col">'. $carbohydrates_lang .'</span>
                                			<span class="list__col">'. $carbohydrates .'</span>
                            			</li>';
                    		endif;

                    		if( !empty( $sugar ) ) :
                    			echo	'<li class="list__row">		                                
                                			<span class="list__col">'. $sugar_lang .'</span>
                                			<span class="list__col">'. $sugar .'</span>
                            			</li>';
                    		endif;

                    		if( !empty( $roughage ) ) :
                    			echo	'<li class="list__row">		                                
                                			<span class="list__col">'. $roughage_lang .'</span>
                                			<span class="list__col">'. $roughage .'</span>
                            			</li>';
                    		endif;

                    		if( !empty( $protein ) ) :
                    			echo	'<li class="list__row">		                                
                                			<span class="list__col">'. $protein_lang .'</span>
                                			<span class="list__col">'. $protein .'</span>
                            			</li>';
                    		endif;

                    		if( !empty( $salt ) ) :
                    			echo	'<li class="list__row">		                                
                                			<span class="list__col">'. $salt_lang .'</span>
                                			<span class="list__col">'. $salt .'</span>
                            			</li>';
                    		endif;

                    		if( !empty( $niacin ) ) :
                    			echo	'<li class="list__row">		                                
                                			<span class="list__col">'. $niacin_lang .'</span>
                                			<span class="list__col">'. $niacin .'</span>
                            			</li>';
                    		endif;

                    		if( !empty( $pantothenic_acid ) ) :
                    			echo	'<li class="list__row">		                                
                                			<span class="list__col">'. $pantothenic_acid_lang .'</span>
                                			<span class="list__col">'. $pantothenic_acid .'</span>
                            			</li>';
                    		endif;

                    		if( !empty( $vitamin_b6 ) ) :
                    			echo	'<li class="list__row">		                                
                                			<span class="list__col">'. $vitamin_b6_lang .'</span>
                                			<span class="list__col">'. $vitamin_b6 .'</span>
                            			</li>';
                    		endif;

                    		if( !empty( $vitamin_b12 ) ) :
                    			echo	'<li class="list__row">		                                
                                			<span class="list__col">'. $vitamin_b12_lang .'</span>
                                			<span class="list__col">'. $vitamin_b12 .'</span>
                            			</li>';
                    		endif;                    		
                        	?>                      	
                        </ul>
						<?php	                        
						if ( is_main_site() ) {
							echo '<span class="copy u-text-sm">*Durchschnittliche Nährwerte pro 100ml fertig zubereitetes Getränk</span>';
						} else {
							echo '<span class="copy u-text-sm">*average content per 100ml of diluted concentrate</span>';
						}
						?>	                        
					</section>
				</div>
				<div class="col-xs-12 col-md-7">
					<div class="product-description">
						<?php
						if ( is_main_site() ) {
							echo '<h4 class="h6">Zutaten:</h4>';
						} else {
							echo '<h4 class="h6">Ingredients:</h4>';
						}
						?>												
						<div class="copy">    								
							<?php the_content(); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>	

	<footer class="product-section hide">
		<div class="container">			
			<?php sodapop_entry_footer(); ?>
		</div>
	</footer><!-- .entry-footer -->
</div><!-- #product-<?php the_ID(); ?> -->
