<?php
/**
 * Template part for displaying accessory
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sodapop
 */

?>

<?php	
	$specs_model 	   = get_field('model_number');
	$specs_measurments = get_field('measurments');
	$specs_weight 	   = get_field('weight');
	$specs_material    = get_field('material');

	$image = get_field('image');
	$video = get_field('video');	

	$bg_color = get_field('background_color');
?>

<div id="product-<?php the_ID(); ?>" class="product">

	<section class="product-section product-intro-section"
	    <?php
	    if( !empty( $bg_color ) ) :
	        echo 'style="
	        		background-color: '. $bg_color .'; 
	        	"';
	    endif;
	    ?> 
	>   			
		<div class="container">
			<div class="row u-flexbox u-flex-align-center u-reverse-mobile">
				<div class="col-xs-12 col-sm-6 col-md-5">	    					
					<header class="product-headline">
    					<h1 class="h2">
    						<?php the_title(); ?>
    					</h1>
					</header>					
                	<div class="product-summary">
                		<div class="copy">
                			<?php the_content(); ?>
                		</div>
                	</div>
	            	<?php
	            		if( !empty( $specs_model ) || 
	            			!empty( $specs_measurments ) ||
	            			!empty( $specs_weight ) ||
	            			!empty( $specs_material ) ) : ?>
							<section class="product-specs">    																
								<?php 
								if ( is_main_site() ) {
									echo '<h4 class="h6">Produktbeschreibung</h4>';
								} else {
									echo '<h4 class="h6">Specifications</h4>';									
								}
								?>									
		                        <ul class="list spec-list">
		                        	<?php
		                        		if( !empty( $specs_model ) ) : ?>
			                            <li class="list__row">			                                
											<?php 
											if ( is_main_site() ) {
												echo '<span class="list__col">Modellnummer:</span>';
											} else {
												echo '<span class="list__col">Model number:</span>';									
											}
											?>				                                
			                                <span class="list__col">
			                                	<?php echo $specs_model; ?>
			                                </span>
			                            </li>
			                        <?php 
			                        endif;
			                        	if( !empty( $specs_measurments ) ) : ?>
			                            <li class="list__row">
											<?php 
											if ( is_main_site() ) {
												echo '<span class="list__col">Abmessungen:</span>';
											} else {
												echo '<span class="list__col">Measurments:</span>';									
											}
											?>			                                
			                                <span class="list__col">
			                                	<?php echo $specs_measurments; ?>
			                                </span>
			                            </li>
			                        <?php
			                    	endif;
			                    		if( !empty( $specs_weight ) ) : ?>
			                            <li class="list__row">
											<?php 
											if ( is_main_site() ) {
												echo '<span class="list__col">Gewicht:</span>';
											} else {
												echo '<span class="list__col">Weight:</span>';									
											}
											?>				                                
			                                <span class="list__col">
			                                	<?php echo $specs_weight; ?>
			                                </span>
			                            </li>
			                        <?php
			                    	endif;
			                    		if( !empty( $specs_material ) ) : ?>          
			                            <li class="list__row">			                                
											<?php 
											if ( is_main_site() ) {
												echo '<span class="list__col">Material:</span>';
											} else {
												echo '<span class="list__col">Material:</span>';									
											}
											?>			                                
			                                <span class="list__col">
			                                	<?php echo $specs_material; ?>
			                                </span>
			                            </li>
			                        <?php
			                    	endif; ?>
		                        </ul>                                                               
							</section>
						<?php
						endif; ?>             	
				</div>
				<?php
				if( !empty( $image ) ) : ?>
					<div class="col-xs-12 col-sm-6 col-md-7">
						<figure class="product-image u-img-center">
							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

							<?php
							if( !empty( $video ) ) : ?>
								<div class="video">								
									<button type="button" class="btn btn-play" data-toggle="modal" data-target="#video">
										<span class="btn-play__icon"></span>
										<?php 
										if ( is_main_site() ) {
											echo '<span class="btn-play__text">Video abspielen</span>';
										} else {
											echo '<span class="btn-play__text">Play Video</span>';									
										}
										?>																				
									</button>
									<div class="video-src hide">
										<?php echo $video; ?>
									</div>
								</div>
							<?php
							endif; ?>
						</figure>
					</div>
				<?php
				endif; ?>
			</div>
		</div>
	</section>

	<?php get_template_part( 'banner' ); ?>

</div><!-- #product-<?php the_ID(); ?> -->