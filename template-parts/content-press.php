<?php
/**
 * Template part for displaying press release post
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sodapop
 */

?>

<?php
	$has_thumb = 'col-xs-12 col-md-8 col-md-offset-2';
	if ( has_post_thumbnail() ) {
		$has_thumb = 'col-xs-12 col-md-6';		
	}

	$download = get_field('download_file');
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="page-section">
		<div class="container">
			<div class="row">
				<?php if ( has_post_thumbnail() ) :
					echo '<div class="col-xs-12 col-md-4 col-md-offset-1"><figure class="u-img-center">'. get_the_post_thumbnail() .'</figure></div>';
				endif; ?>				
				<div class="<?php echo $has_thumb; ?>">
					<header class="article-header">
                        <div class="badge press-card__date">
                            <span>
                            	<?php echo get_the_date('d.m.Y'); ?>
                            </span>
                        </div>						
						<?php
						if ( is_singular() ) :
							the_title( '<h1 class="entry-title h4">', '</h1>' );
						else :
							the_title( '<h2 class="entry-title h4"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
						endif;

						if ( 'post' === get_post_type() ) : ?>
						<div class="entry-meta">
							<?php sodapop_posted_on(); ?>
						</div><!-- .entry-meta -->
						<?php
						endif; ?>
					</header><!-- .entry-header -->											
					<div class="copy">
						<?php
							the_content( sprintf(
								wp_kses(
									/* translators: %s: Name of current post. Only visible to screen readers */
									__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'sodapop' ),
									array(
										'span' => array(
											'class' => array(),
										),
									)
								),
								get_the_title()
							) );

							wp_link_pages( array(
								'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'sodapop' ),
								'after'  => '</div>',
							) );
						?>
					</div>
                    <?php
                    if( !empty( $download ) ) :                    
                    	echo '<a href="'. $download .'" target="_blank" class="btn btn-default btn-download">Download press release</a>';
    				endif; 
    				?>											
				</div>
			</div>
		</div><!-- .container -->
	</div><!-- .page-section -->

	<footer class="entry-footer">
		<div class="container">			
			<?php sodapop_entry_footer(); ?>
		</div>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->