<?php
/**
 * Template part for displaying results in search pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sodapop
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('row'); ?>>	
	<?php 
	if ( has_post_thumbnail() ) :
		echo    '<figure class="col-xs-3 col-sm-3 col-md-2">
					'. get_the_post_thumbnail() .'
				</figure>';
	endif; ?>
	<header class="entry-header 
		<?php 
		if ( has_post_thumbnail() ) { 
			echo 'col-xs-9 col-sm-9 col-md-10'; 
		} else { 
			echo 'col-xs-9 col-xs-offset-3 col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2'; } 
		?>">
		<?php the_title( sprintf( '<h2 class="h4"><a href="%s" rel="bookmark" class="u-color-primary">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>

		<?php if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php sodapop_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php endif; ?>

		<div class="entry-summary">
			<?php echo content(30); ?>
		</div><!-- .entry-summary -->		
	</header><!-- .entry-header -->	

	<footer class="entry-footer hide">
		<?php sodapop_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
