<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sodapop
 */

?>

<header class="page-header">
	<div class="container">
		<div class="text-center">								
			<h1 class="page-title"><?php esc_html_e( 'Nothing Found', 'sodapop' ); ?></h1>
		</div>
	</div>
</header><!-- .page-header -->


<?php
if ( is_home() && current_user_can( 'publish_posts' ) ) : ?>

	<p><?php
		printf(
			wp_kses(
				/* translators: 1: link to WP admin new post page. */
				__( 'Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'sodapop' ),
				array(
					'a' => array(
						'href' => array(),
					),
				)
			),
			esc_url( admin_url( 'post-new.php' ) )
		);
	?></p>

<?php elseif ( is_search() ) : ?>

	<div class="page-section">
		<div class="container">					
			<div class="row">
				<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
					<p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'sodapop' ); ?></p>
					<br>
					<?php
						get_search_form();
					?>
				</div>
			</div>
		</div>
	</div>
<?php		
else : ?>
	
	<div class="page-section">
		<div class="container">					
			<div class="row">
				<div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
					<p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'sodapop' ); ?></p>
					<br>
					<?php
						get_search_form();
					?>
				</div>
			</div>
		</div>
	</div>
<?php
endif; ?>

