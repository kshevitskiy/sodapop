<?php
/**
 * Template Name: Bottle Sleeves
 *
 * The template for displaying bottle sleeves page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package sodapop
 */
get_header(); ?>

	<?php
		$bg_color   = get_field('header_background_color');
		$background = '';
		if( !empty( $bg_color )) {
		    $background = 'style="background-color: '. $bg_color .';"';
		}
	?>	

	<main id="main" class="site-main">
		<?php
			// Page header
			get_template_part( 'page-header' );

			echo 	'<figure class="u-img-center" '. $background .'>
						<img src="'. get_template_directory_uri() .'/img/bottle-sleeves.jpg" alt="Bottle Sleeves" />
					</figure>';

			while ( have_posts() ) : the_post(); ?>
		        <div class="page-section">
		            <div class="container">
		                <div class="row">
		                    <div class="col-xs-12 col-md-6 col-md-offset-3">                            
		                        <div class="text-center">                            
		                            <div class="copy u-copy-lg">
		                                <?php the_content(); ?>
		                            </div>
		                        </div>
		                    </div>
		                </div>
		            </div>
		        </div>
			<?php
			endwhile; // End of the loop.

			// Bottle sleeves grid
			get_template_part( 'bottle-sleeves' );
		?>
	</main><!-- #main -->

<?php
get_footer();